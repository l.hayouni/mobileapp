import { call, put, takeEvery,takeLatest } from "redux-saga/effects";
import { 
    addNote,
    deleteNote,
    getNote,
    updateNote,
} from "./api";
import {
  GET_NOTE,
  GET_NOTE_SUCCESS,
  ADD_NOTE,
  REQUEST_SUCCESS,
  DELETE_NOTE,
  UPDATE_NOTE,
} from "./actionsTypes";

export function* getN({ data }) {
    const { id_user,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(getNote, id_user);
      yield put({ type: GET_NOTE_SUCCESS, response});
      if (response!="0") {
        
        successCallBack()
      }
      else{

      }
    } catch (error) {
      alert(error)
      
    }
  }

export function* addN({ data }) {
    const { contenu,date,id_user,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(addNote,contenu,date,id_user);
      yield put({ type: REQUEST_SUCCESS});

      if (response!==0) {
        successCallBack(response)
      }
      else{
        errorCallBack()
      }
    } catch (error) {
      yield put({ type: REQUEST_SUCCESS});
      alert(error)
      
    }
  }
export function* UpdateN({ data }) {
    const { id,contenu,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(updateNote,id,contenu);
      yield put({ type: REQUEST_SUCCESS});

      if (response==1) {
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
      yield put({ type: REQUEST_SUCCESS});
      alert(error)
      
    }
  }
export function* deleteN({ data }) {
    const { list,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(deleteNote,list);
      yield put({ type: REQUEST_SUCCESS});

      if (response==1) {
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
        alert('ERREUR')
      
    }
  }
export function* actionWatcherNote(){
    yield takeEvery(GET_NOTE, getN)
    yield takeEvery(ADD_NOTE, addN)
    yield takeEvery(UPDATE_NOTE,UpdateN)
    yield takeEvery(DELETE_NOTE, deleteN)
}
