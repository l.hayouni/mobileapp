import{
    GET_NOTE,
    ADD_NOTE,
    DELETE_NOTE,
    UPDATE_NOTE,
}from './actionsTypes'

export const getNote=(id_user,successCallBack,errorCallBack)=>(
    {
        type:GET_NOTE,
        data:{id_user,successCallBack,errorCallBack}
    }
)

export const addNote=(contenu,date,id_user,successCallBack,errorCallBack)=>(
    {
        type:ADD_NOTE,
        data:{contenu,date,id_user,successCallBack,errorCallBack}
    }
)
export const updateNote=(id,contenu,successCallBack,errorCallBack)=>(
    {
        type:UPDATE_NOTE,
        data:{id,contenu,successCallBack,errorCallBack}
    }
)
export const deleteNote=(list,successCallBack,errorCallBack)=>(
    {
        type:DELETE_NOTE,
        data:{list,successCallBack,errorCallBack}
    }
)

