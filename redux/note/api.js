import { SERVER_API_URL } from "../../util/constants";

export const getNote = (id_user) => {
    return fetch(SERVER_API_URL+'/Note.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id_user:id_user
        })

    })
    .then(response => response.json())};
export const addNote = (contenu,date,id_user) => {
    return fetch(SERVER_API_URL+'/AddNote.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            contenu:contenu,
            date:date,
            id_user:id_user
        })

    })
    .then(response => response.json())};
export const updateNote = (id,contenu) => {
    return fetch(SERVER_API_URL+'/UpdateNote.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id:id,
            contenu:contenu,
        })

    })
    .then(response => response.json())};
export const deleteNote = (list) => {
    return fetch(SERVER_API_URL+'/DeleteNote.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            list:list,
        })

    })
        .then(response => response.json())};