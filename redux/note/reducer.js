import {
    GET_NOTE_SUCCESS,
    GET_NOTE,
    ADD_NOTE,
    UPDATE_NOTE,
    REQUEST_SUCCESS,
    DELETE_NOTE,
  } from "./actionsTypes";
const initialState={
    isLoading:false,
    note:null,
}
export default(state=initialState,action)=>{
    let nextState
    switch(action.type){
        case GET_NOTE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case GET_NOTE_SUCCESS:
            nextState={
                ...state,
                note:action.response,
                isLoading:false
            }
            return nextState || state
        case ADD_NOTE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case UPDATE_NOTE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case DELETE_NOTE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case REQUEST_SUCCESS:
            nextState={
                ...state,
                isLoading:false
            }
            return nextState || state
        default: 
            return state;
            
    }
};