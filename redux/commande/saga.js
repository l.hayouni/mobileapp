import { call, put, takeEvery,takeLatest } from "redux-saga/effects";
import { 
    addProduct,
    deleteProduct,
    getUserCommande,
    verifyCommande,
    getFacture,
    getTotal
} from "./api";
import {
  GET_COMMANDE,
  GET_COMMANDE_SUCCESS,
  ADD_COMMANDE_PRODUCT,
  ADD_COMMANDE_SUCCESS,
  DELETE_COMMANDE_PRODUCT,
  VERIFY_COMMANDE,
  GET_FACTURE,
  GET_TOTAL
} from "./actionsTypes";

export function* getCommande({ data }) {
    const { successCallBack,errorCallBack,id, date } = data;
    try {
      const response = yield call(getUserCommande, id,date);
      if (response!="0") {
        
        yield put({ type: GET_COMMANDE_SUCCESS, response});
        successCallBack(response);
      }
    } catch (error) {
      alert(error)
      
    }
  }
export function* addCommandeProduct({ data }) {
    const { id,id_produit,quantite, date,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(addProduct, id,id_produit,quantite,date);
      if (response==1) {
        yield put({ type: ADD_COMMANDE_SUCCESS});
        successCallBack()
      }
      else{
        yield put({ type: ADD_COMMANDE_SUCCESS});
        errorCallBack()
      }
    } catch (error) {
      yield put({ type: ADD_COMMANDE_SUCCESS});
      alert('ERREUR')
      
    }
  }
export function* deleteCommandeProduct({ data }) {
    const { id,id_produit, date,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(deleteProduct, id,id_produit,date);
      if (response==1) {
        yield put({ type: ADD_COMMANDE_SUCCESS});
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
      alert('ERREUR')
      
    }
  }
export function* verifyC({ data}) {
    const { date,successCallBack } = data;
    try {
      const response = yield call(verifyCommande,date);
      successCallBack(response.total)
      yield put({ type: ADD_COMMANDE_SUCCESS});

    } catch (error) {
      
      alert(error)
      
    }
  }
export function* getF({ data}) {
    const { month,year,successCallBack } = data;
    try {
      const response = yield call(getFacture,month,year);
      successCallBack(response)
      yield put({ type: ADD_COMMANDE_SUCCESS});

    } catch (error) {
      
      alert("Erreur")
      
    }
  }
export function* getT({ data }) {
    const { date,id } = data;
    try {
      const response = yield call(getTotal, date,id);
      yield put({ type: ADD_COMMANDE_SUCCESS});
      if (response!="0") {
        alert("Total="+(response.produits-response.charges-response.virement)+ ' Franc')
      }
      else{
        alert('Erreur')
      }
    } catch (error) {
        alert(error)
      
    }
  }
export function* actionWatcherCommande(){
    yield takeEvery(GET_COMMANDE, getCommande)
    yield takeEvery(ADD_COMMANDE_PRODUCT, addCommandeProduct)
    yield takeEvery(DELETE_COMMANDE_PRODUCT, deleteCommandeProduct)
    yield takeLatest(VERIFY_COMMANDE, verifyC)
    yield takeLatest(GET_FACTURE, getF)
    yield takeLatest(GET_TOTAL, getT)
}
