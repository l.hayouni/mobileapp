import { SERVER_API_URL } from "../../util/constants";

export const getUserCommande = (id,date) => {
    return fetch(SERVER_API_URL+'/DayCommande.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            date:date,
            id:id
        })

    })
    .then(response => response.json())};
export const addProduct = (id,id_produit,quantite,date,successCallBack,errorCallBack) => {
    return fetch(SERVER_API_URL+'/AddCommandeProduct.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id:id,
            id_produit:id_produit,
            quantite:quantite,
            date:date
        })

    })
    .then(response => response.json())};
export const deleteProduct = (id,id_produit,date,successCallBack,errorCallBack) => {
    return fetch(SERVER_API_URL+'/DelCommandeProd.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id_produit:id_produit,
            date:date,
            id:id
        })

    })
        .then(response => response.json())};
export const verifyCommande = (date) => {
    return fetch(SERVER_API_URL+'/Verification.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            date:date,
        })

    })
        .then(response => response.json())};
export const getFacture = (month,year) => {
    return fetch(SERVER_API_URL+'/Facture.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            month:month,
            year:year
        })

    })
        .then(response => response.json())};
export const getTotal = (date,id) => {
    return fetch(SERVER_API_URL+'/TotalProduit.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            date:date,
            id:id
        })

    })
        .then(response => response.json())};