import {
    GET_COMMANDE_SUCCESS,
    GET_COMMANDE,
    ADD_COMMANDE_PRODUCT,
    ADD_COMMANDE_SUCCESS,
    DELETE_COMMANDE_PRODUCT,
    VERIFY_COMMANDE,
    GET_FACTURE,
    GET_TOTAL
  } from "./actionsTypes";
const initialState={
    isLoading:false,
    commande:null,
    productModal:false
}
export default(state=initialState,action)=>{
    let nextState
    switch(action.type){
        case GET_COMMANDE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case GET_COMMANDE_SUCCESS:
            nextState={
                ...state,
                commande:action.response,
                isLoading:false
            }
            return nextState || state
        case ADD_COMMANDE_PRODUCT:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case DELETE_COMMANDE_PRODUCT:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case ADD_COMMANDE_SUCCESS:
            nextState={
                ...state,
                isLoading:false
            }
            return nextState || state
        case VERIFY_COMMANDE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case GET_FACTURE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case GET_TOTAL:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        default: 
            return state;
            
    }
};