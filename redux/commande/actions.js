import{
    GET_COMMANDE,
    ADD_COMMANDE_PRODUCT,
    DELETE_COMMANDE_PRODUCT,
    VERIFY_COMMANDE,
    GET_FACTURE,
    GET_TOTAL
}from './actionsTypes'

export const getCommande=(successCallBack,errorCallBack,id,date)=>(
    {
        type:GET_COMMANDE,
        data:{successCallBack,errorCallBack,id,date}
    }
)
export const addCommandeProduct=(id,id_produit,quantite,date,successCallBack,errorCallBack)=>(
    {
        type:ADD_COMMANDE_PRODUCT,
        data:{id,id_produit,quantite,date,successCallBack,errorCallBack}
    }
)
export const deleteCommandeProduct=(id,id_produit,date,successCallBack,errorCallBack)=>(
    {
        type:DELETE_COMMANDE_PRODUCT,
        data:{id,id_produit,date,successCallBack,errorCallBack}
    }
)
export const verify=(date,successCallBack)=>(
    {
        type:VERIFY_COMMANDE,
        data:{date,successCallBack}
    }
)
export const getFacture=(month,year,successCallBack)=>(
    {
        type:GET_FACTURE,
        data:{month,year,successCallBack}
    }
)
export const getTotal=(date,id,successCallBack)=>(
    {
        type:GET_TOTAL,
        data:{date,id,successCallBack}
    }
)
