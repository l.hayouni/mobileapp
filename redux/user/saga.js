import { call, put, takeEvery } from "redux-saga/effects";
import { 
  getUsers,
  deleteUser,
  addUser,
  getClients,
  deleteClient,
  addClient
} from "./api";
import {
  GET_USERS,
  GET_USERS_SUCCESS,
  DELETE_USER,
  REQUEST_SUCCESS,
  ADD_USER,
  GET_CLIENTS,
  GET_CLIENTS_SUCCESS,
  ADD_CLIENT,
  DELETE_CLIENT
} from "./actionsTypes";

export function* getU({data}) {
    const {role,successCallBack,errorCallBack}=data
    try {
      const response = yield call(getUsers,role);
      yield put({ type: GET_USERS_SUCCESS, response});

      if (response[0]!=undefined) {
        successCallBack()
      }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
        alert("ERREUR")
    }
  }
export function* addU({data}) {
    const {username,password,role,id,successCallBack,errorCallBack}=data
    try {
        const response = yield call(addUser,username,password,role,id);
        yield put({ type: REQUEST_SUCCESS});
        if(response=="ajouté")
        {
            successCallBack()
        }
        else{
            alert(response)
        }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
    
        alert(error)
    }
  }
export function* deleteU({data}) {
    const {list,successCallBack,errorCallBack}=data
    try {
      const response = yield call(deleteUser,list);
      yield put({ type: REQUEST_SUCCESS});
      if (response==1) {
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
        alert('ERREUR')
      
    }
  }
  export function* getC({data}) {
    const {id,successCallBack,errorCallBack}=data
    try {
      const response = yield call(getClients,id);
      yield put({ type: GET_CLIENTS_SUCCESS, response});

      if (response!=undefined) {
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
        alert(error)
    }
  }
export function* addC({data}) {
    const {nom,id,successCallBack,errorCallBack}=data
    try {
        const response = yield call(addClient,nom,id);
        yield put({ type: REQUEST_SUCCESS});

        if(response==1)
        {
          successCallBack()
        }
        else {
          errorCallBack()
        }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
    
        alert("ERREUR")
    }
  }
export function* deleteC({data}) {
    const {list,successCallBack,errorCallBack}=data
    try {
      const response = yield call(deleteClient,list);
      yield put({ type: REQUEST_SUCCESS});

      if (response==1) {
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
        alert('ERREUR')
      
    }
  }
export function* actionWatcherUsers(){
    yield takeEvery(GET_USERS, getU)
    yield takeEvery(ADD_USER, addU)
    yield takeEvery(DELETE_USER, deleteU)
    yield takeEvery(GET_CLIENTS, getC)
    yield takeEvery(ADD_CLIENT, addC)
    yield takeEvery(DELETE_CLIENT, deleteC)
}