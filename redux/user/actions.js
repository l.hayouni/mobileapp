import{
    GET_USERS,
    DELETE_USER,
    ADD_USER,
    GET_CLIENTS,
    DELETE_CLIENT,
    ADD_CLIENT
}from './actionsTypes'

export const getClients=(id,successCallBack,errorCallBack)=>(
    {
        type:GET_CLIENTS,
        data:{id,successCallBack,errorCallBack}
    }
)
export const getUsers=(role,successCallBack,errorCallBack)=>(
    {
        type:GET_USERS,
        data:{role,successCallBack,errorCallBack}
    }
)
export const addUser=(username,password,role,id,successCallBack,errorCallBack)=>(
    {
        type:ADD_USER,
        data:{username,password,role,id,successCallBack,errorCallBack}
    }
)
export const addClient=(nom,id,successCallBack,errorCallBack)=>(
    {
        type:ADD_CLIENT,
        data:{nom,id,successCallBack,errorCallBack}
    }
)
export const deleteUser=(list,successCallBack,errorCallBack)=>(
    {
        type:DELETE_USER,
        data:{list,successCallBack,errorCallBack}
    }
)
export const deleteClient=(list,successCallBack,errorCallBack)=>(
    {
        type:DELETE_CLIENT,
        data:{list,successCallBack,errorCallBack}
    }
)