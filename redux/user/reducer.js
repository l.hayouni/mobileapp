import {
    GET_USERS,
    GET_USERS_SUCCESS,
    DELETE_USER,
    REQUEST_SUCCESS,
    ADD_USER,
    GET_CLIENTS,
    GET_CLIENTS_SUCCESS,
    ADD_CLIENT,
    DELETE_CLIENT,
  } from "./actionsTypes";
const initialState={
    isLoading:false,
    users:[],
    clients:[]
}
export default(state=initialState,action)=>{
    let nextState
    switch(action.type){
        case GET_USERS:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case ADD_USER:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case GET_USERS_SUCCESS:
            nextState={
                ...state,
                users: action.response,
                isLoading: false
            }
            return nextState || state
        case DELETE_USER:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case GET_CLIENTS:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case ADD_CLIENT:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case GET_CLIENTS_SUCCESS:
            nextState={
                ...state,
                clients: action.response,
                isLoading: false
            }
            return nextState || state
        case DELETE_CLIENT:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case REQUEST_SUCCESS:
            nextState={
                ...state,
                isLoading: false
            }
            return nextState || state
        default: 
            return state;
            
    }
};