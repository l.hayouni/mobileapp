import { SERVER_API_URL } from "../../util/constants";

export const getUsers = (role) => {
    return fetch(SERVER_API_URL+'Users.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            role:role
        })

    })
          .then(response => response.json())
  };
export const addUser = (username,password,role,id) => {
    return fetch(SERVER_API_URL+'Signup.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            email:username,
            password:password,
            role:role,
            id:id
        })

    })
          .then(response => response.json())
  };
export const deleteUser = (list) => {
    return fetch(SERVER_API_URL+'/DeleteUser.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            list:list
        })
      })
      .then(response => response.json())
};
export const getClients = (id) => {
    return fetch(SERVER_API_URL+'Clients.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id:id
        })

    })
          .then(response => response.json())
  };
export const addClient = (nom,id) => {
    return fetch(SERVER_API_URL+'AddClient.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            nom:nom,
            id:id,
        })

    })
          .then(response => response.json())
  };
export const deleteClient = (list) => {
    return fetch(SERVER_API_URL+'/DeleteClient.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            list:list
        })
      })
      .then(response => response.json())
};