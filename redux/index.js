import { combineReducers } from "redux";


import auth from "./auth/reducer";
import produit from "./product/reducer";
import commande from "./commande/reducer";
import change from "./change/reducer";
import user from "./user/reducer";
import charge from "./charge/reducer";
import note from "./note/reducer";

export default combineReducers({
  auth,
  produit,
  commande,
  change,
  user,
  charge,
  note
});
