import { call, put, takeEvery,takeLatest } from "redux-saga/effects";
import { 
    addCharge,
    deleteCharge,
    getCharge,
    updateCharge,
    getVirement
} from "./api";
import {
  GET_CHARGE,
  GET_CHARGE_SUCCESS,
  ADD_CHARGE,
  REQUEST_SUCCESS,
  DELETE_CHARGE,
  UPDATE_CHARGE,
  GET_VIREMENT
} from "./actionsTypes";

export function* getC({ data }) {
    const { id, date,successCallBack } = data;
    try {
      const response = yield call(getCharge, id,date);
      if (response!="0") {
        yield put({ type: GET_CHARGE_SUCCESS, response});
        successCallBack()
      }
    } catch (error) {
      alert(error)
      
    }
  }
export function* getV({ data }) {
    const { id, date,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(getVirement, id,date);
      yield put({ type: REQUEST_SUCCESS});
      if (response!="0") {
        successCallBack(response[0])
      }
      else{
        errorCallBack()
      }
    } catch (error) {
        errorCallBack()
      
    }
  }
export function* addC({ data }) {
    const { id,titre,total,date,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(addCharge,id,titre,total,date);
      yield put({ type: REQUEST_SUCCESS});

      if (response==1) {
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
      yield put({ type: REQUEST_SUCCESS});
      alert(error)
      
    }
  }
export function* UpdateC({ data }) {
    const { id,titre,total,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(updateCharge,id,titre,total);
      yield put({ type: REQUEST_SUCCESS});

      if (response==1) {
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
      yield put({ type: REQUEST_SUCCESS});
      alert(error)
      
    }
  }
export function* deleteC({ data }) {
    const { list,date,id,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(deleteCharge,list,date,id);
      yield put({ type: REQUEST_SUCCESS});

      if (response==1) {
        successCallBack()
      }
      else{
        errorCallBack()
      }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
        alert('ERREUR')
      
    }
  }
export function* actionWatcherCharge(){
    yield takeEvery(GET_CHARGE, getC)
    yield takeEvery(GET_VIREMENT, getV)
    yield takeEvery(ADD_CHARGE, addC)
    yield takeEvery(UPDATE_CHARGE,UpdateC)
    yield takeEvery(DELETE_CHARGE, deleteC)
}
