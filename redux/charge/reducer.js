import {
    GET_CHARGE_SUCCESS,
    GET_CHARGE,
    ADD_CHARGE,
    UPDATE_CHARGE,
    REQUEST_SUCCESS,
    DELETE_CHARGE,
    GET_VIREMENT
  } from "./actionsTypes";
const initialState={
    isLoading:false,
    charge:null,
}
export default(state=initialState,action)=>{
    let nextState
    switch(action.type){
        case GET_CHARGE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case GET_VIREMENT:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case GET_CHARGE_SUCCESS:
            nextState={
                ...state,
                charge:action.response,
                isLoading:false
            }
            return nextState || state
        case ADD_CHARGE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case UPDATE_CHARGE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case DELETE_CHARGE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case REQUEST_SUCCESS:
            nextState={
                ...state,
                isLoading:false
            }
            return nextState || state
        default: 
            return state;
            
    }
};