import { SERVER_API_URL } from "../../util/constants";

export const getCharge = (id,date) => {
    return fetch(SERVER_API_URL+'/Charge.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            date:date,
            id:id
        })

    })
    .then(response => response.json())};
export const getVirement = (id,date) => {
    return fetch(SERVER_API_URL+'/Virement.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            date:date,
            id:id
        })

    })
    .then(response => response.json())};
export const addCharge = (id,titre,total,date,successCallBack,errorCallBack) => {
    return fetch(SERVER_API_URL+'/AddCharge.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id:id,
            titre:titre,
            total:total,
            date:date
        })

    })
    .then(response => response.json())};
export const updateCharge = (id,titre,total,successCallBack,errorCallBack) => {
    return fetch(SERVER_API_URL+'/UpdateCharge.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id_charge:id,
            titre:titre,
            total:total,
        })

    })
    .then(response => response.json())};
export const deleteCharge = (list,date,id,successCallBack,errorCallBack) => {
    return fetch(SERVER_API_URL+'/DeleteCharge.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            list:list,
            date:date,
            id:id
        })

    })
        .then(response => response.json())};