import{
    GET_CHARGE,
    ADD_CHARGE,
    DELETE_CHARGE,
    UPDATE_CHARGE,
    GET_VIREMENT
}from './actionsTypes'

export const getCharge=(id,date,successCallBack)=>(
    {
        type:GET_CHARGE,
        data:{id,date,successCallBack}
    }
)
export const getVirement=(id,date,successCallBack,errorCallBack)=>(
    {
        type:GET_VIREMENT,
        data:{id,date,successCallBack,errorCallBack}
    }
)
export const addCharge=(id,titre,total,date,successCallBack,errorCallBack)=>(
    {
        type:ADD_CHARGE,
        data:{id,titre,total,date,successCallBack,errorCallBack}
    }
)
export const updateCharge=(id,titre,total,successCallBack,errorCallBack)=>(
    {
        type:UPDATE_CHARGE,
        data:{id,titre,total,successCallBack,errorCallBack}
    }
)
export const deleteCharge=(list,date,id,successCallBack,errorCallBack)=>(
    {
        type:DELETE_CHARGE,
        data:{list,date,id,successCallBack,errorCallBack}
    }
)

