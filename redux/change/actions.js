import{
    GET_CHANGE,
    ADD_CHANGE_PRODUCT,
    DELETE_CHANGE_PRODUCT,
}from './actionsTypes'

export const getChange=(successCallBack,errorCallBack,id,date)=>(
    {
        type:GET_CHANGE,
        data:{successCallBack,errorCallBack,id,date}
    }
)
export const addChangeProduct=(id,id_produit,quantite,date,successCallBack,errorCallBack)=>(
    {
        type:ADD_CHANGE_PRODUCT,
        data:{id,id_produit,quantite,date,successCallBack,errorCallBack}
    }
)
export const deleteChangeProduct=(id,id_produit,date,successCallBack,errorCallBack)=>(
    {
        type:DELETE_CHANGE_PRODUCT,
        data:{id,id_produit,date,successCallBack,errorCallBack}
    }
)