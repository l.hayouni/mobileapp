import {
    GET_CHANGE_SUCCESS,
    GET_CHANGE,
    ADD_CHANGE_PRODUCT,
    REQUEST_SUCCESS,
    DELETE_CHANGE_PRODUCT,
  } from "./actionsTypes";
const initialState={
    isLoading:false,
    change:null,
}
export default(state=initialState,action)=>{
    let nextState
    switch(action.type){
        case GET_CHANGE:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case GET_CHANGE_SUCCESS:
            nextState={
                ...state,
                change:action.response,
                isLoading:false
            }
            return nextState || state
        case ADD_CHANGE_PRODUCT:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case DELETE_CHANGE_PRODUCT:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case REQUEST_SUCCESS:
            nextState={
                ...state,
                isLoading:false
            }
            return nextState || state
        
        default: 
            return state;
            
    }
};