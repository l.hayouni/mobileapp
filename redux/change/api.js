import { SERVER_API_URL } from "../../util/constants";

export const getClientChange = (id,date) => {
    return fetch(SERVER_API_URL+'/DayChange.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            date:date,
            id:id
        })

    })
    .then(response => response.json())};
export const addProduct = (id,id_produit,quantite,date,successCallBack,errorCallBack) => {
    return fetch(SERVER_API_URL+'/AddChangeProduct.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id:id,
            id_produit:id_produit,
            quantite:quantite,
            date:date
        })

    })
    .then(response => response.json())};
export const deleteProduct = (id,id_produit,date,successCallBack,errorCallBack) => {
    return fetch(SERVER_API_URL+'/DelChangeProd.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            id_produit:id_produit,
            date:date,
            id:id
        })

    })
        .then(response => response.json())};
