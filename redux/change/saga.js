import { call, put, takeEvery,takeLatest } from "redux-saga/effects";
import { 
    addProduct,
    deleteProduct,
    getClientChange,
} from "./api";
import {
  GET_CHANGE,
  GET_CHANGE_SUCCESS,
  ADD_CHANGE_PRODUCT,
  REQUEST_SUCCESS,
  DELETE_CHANGE_PRODUCT,
} from "./actionsTypes";

export function* getChange({ data }) {
    const { successCallBack,errorCallBack,id, date } = data;
    try {
      const response = yield call(getClientChange, id,date);
      if (response!="0") {
        yield put({ type: GET_CHANGE_SUCCESS, response});
        successCallBack(response)
      }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS, response});
        alert('ERREUR')
      
    }
  }
export function* addChangeProduct({ data }) {
    const { id,id_produit,quantite, date,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(addProduct, id,id_produit,quantite,date);
      if (response==1) {
        yield put({ type: REQUEST_SUCCESS});
        successCallBack()
      }
      else{
        yield put({ type: REQUEST_SUCCESS});
        errorCallBack()
      }
    } catch (error) {
      yield put({ type: REQUEST_SUCCESS});
      alert('ERREUR')
      
    }
  }
export function* deleteChangeProduct({ data }) {
    const { id,id_produit, date,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(deleteProduct, id,id_produit,date);
      if (response==1) {
        yield put({ type: REQUEST_SUCCESS});
        successCallBack()
      }
      else{
        yield put({ type: REQUEST_SUCCESS});
        errorCallBack()
      }
    } catch (error) {
        yield put({ type: REQUEST_SUCCESS});
        alert('ERREUR')
      
    }
  }
export function* actionWatcherChange(){
    yield takeEvery(GET_CHANGE, getChange)
    yield takeEvery(ADD_CHANGE_PRODUCT, addChangeProduct)
    yield takeEvery(DELETE_CHANGE_PRODUCT, deleteChangeProduct)
}
