import { SERVER_API_URL } from "../../util/constants";

export const login = (username,password) => {
    return fetch(SERVER_API_URL+'/Login.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            email:username,
            password:password
        })

    })
        .then(response => response.json())
  };
export const resetPass = (username,password) => {
    return fetch(SERVER_API_URL+'/UpdateUser.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            email:username,
            password:password,
        })

    })
        .then(response => response.json())
  };