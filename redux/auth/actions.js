import{
    LOGIN_USER,
    RESET_PASS,
    LOG_OUT
}from './actionsTypes'

export const loginUser=(username,password,successCallBack,errorCallBack)=>(
    {
        type:LOGIN_USER,
        data:{username,password,successCallBack,errorCallBack},
    }
)
export const resetPassword=(username,password,successCallBack,errorCallBack)=>(
    {
        type:RESET_PASS,
        data:{username,password,successCallBack,errorCallBack},
    }
)
export const logout=()=>(
    {
        type:LOG_OUT,
    }
)