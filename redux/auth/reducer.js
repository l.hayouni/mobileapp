import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_ERROR,
    RESET_PASS,
    RESET_PASS_SUCCESS,
    RESET_PASS_ERROR,
    LOG_OUT,
  } from "./actionsTypes";
const initialState={
    isLoading:false,
    user:null,
}
export default(state=initialState,action)=>{
    let nextState
    switch(action.type){
        case LOGIN_USER:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case LOGIN_USER_SUCCESS:
            nextState={
                ...state,
                user:action.response,
                isLoading:false
            }
            return nextState || state
        case LOGIN_USER_ERROR:
            nextState={
                ...state,
                isLoading:false
            }
            return nextState || state
         case RESET_PASS:
            nextState={
                ...state,
                isLoading:true
            }
            return nextState || state
        case RESET_PASS_SUCCESS:
            nextState={
                ...state,
                isLoading:false
            }
            return nextState || state
        case RESET_PASS_ERROR:
            nextState={
                ...state,
                isLoading:false
            }
            return nextState || state
        case LOG_OUT:
            nextState={
                ...state,
                user:null
            }
            return nextState || state
        default: 
            return state;
            
    }
};