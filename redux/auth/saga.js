import { call, put, takeEvery } from "redux-saga/effects";
import { 
  login,
  resetPass,
} from "./api";
import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  RESET_PASS,
  RESET_PASS_SUCCESS,
  RESET_PASS_ERROR,
} from "./actionsTypes";

export function* logg({ data }) {
    const { username, password,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(login, username,password);
      if (response!=0) {
        yield put({ type: LOGIN_USER_SUCCESS, response});
        successCallBack();
      }
      else{
        yield put({ type: LOGIN_USER_ERROR});
        errorCallBack();
      }
    } catch (error) {
      yield put({ type: LOGIN_USER_ERROR});
      
    }
  }
export function* reset({ data }) {
    const { username, password,successCallBack,errorCallBack } = data;
    try {
      const response = yield call(resetPass, username,password);
      if (response==1) {
        yield put({ type: RESET_PASS_SUCCESS, response});
        successCallBack();
      }
      else{
        yield put({ type: RESET_PASS_ERROR});
        errorCallBack();
      }
    } catch (error) {
      yield put({ type: RESET_PASS_ERROR});
      
    }
  }
export function* actionWatcherAuth(){
    yield takeEvery(LOGIN_USER, logg)
    yield takeEvery(RESET_PASS, reset)
}