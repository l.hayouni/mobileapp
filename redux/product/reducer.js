import {
    GET_PRODUCT_SUCCESS,
    GET_COMMANDE_PRODUCT_SUCCESS,
    GET_PRODUCT_ERROR,
    GET_PRODUCT,
    GET_COMMANDE_PRODUCT,
    ADD_PRODUCT,
    ADD_PRODUCT_SUCCESS,
    ADD_PRODUCT_ERROR,
    EDIT_PRODUCT,
    EDIT_PRODUCT_SUCCESS,
    EDIT_PRODUCT_ERROR,
    DELETE_PRODUCT,
    DELETE_PRODUCT_SUCCESS,
    DELETE_PRODUCT_ERROR
  } from "./actionsTypes";
const initialState={
    isLoading:false,
    produits:[],
    commandeProduits:[]
}
export default(state=initialState,action)=>{
    let nextState
    switch(action.type){
        case GET_PRODUCT:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case GET_COMMANDE_PRODUCT:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case ADD_PRODUCT:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case ADD_PRODUCT_SUCCESS:
            nextState={
                ...state,
                isLoading: false
            }
            return nextState || state
        case ADD_PRODUCT_ERROR:
            nextState={
                ...state,
                isLoading: false
            }
            return nextState || state
        case EDIT_PRODUCT:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case EDIT_PRODUCT_SUCCESS:
            nextState={
                ...state,
                isLoading: false
            }
            return nextState || state
        case EDIT_PRODUCT_ERROR:
            nextState={
                ...state,
                isLoading: false
            }
            return nextState || state
        case DELETE_PRODUCT:
            nextState={
                ...state,
                isLoading: true
            }
            return nextState || state
        case DELETE_PRODUCT_SUCCESS:
            nextState={
                ...state,
                isLoading: false
            }
            return nextState || state
        case DELETE_PRODUCT_ERROR:
            nextState={
                ...state,
                isLoading: false
            }
            return nextState || state
        case GET_PRODUCT_SUCCESS:
            nextState={
                ...state,
                produits: action.response,
                isLoading:false
            }
            return nextState || state
        case GET_COMMANDE_PRODUCT_SUCCESS:
            nextState={
                ...state,
                commandeProduits: action.response,
                isLoading:false
            }
            return nextState || state
        case GET_PRODUCT_ERROR:
            nextState={
                ...state,
                isLoading: false
            }
            return nextState || state
        default: 
            return state;
            
    }
};