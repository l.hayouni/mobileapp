import { SERVER_API_URL } from "../../util/constants";

export const getProduct = () => {
    return fetch(SERVER_API_URL+'/Produits.php')
    .then(response => response.json())
  };
export const getCommandeProduct = (list) => {
    return fetch(SERVER_API_URL+'/CommandeProducts.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            list:list,
        })

    })
        .then(response => response.json())
  };
export const newProduct = (nom,prix) => {
    return fetch(SERVER_API_URL+'/AddProduct.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            nom:nom,
            prix:prix
        })

    })
        .then(response => response.json())
  };
export const updateProduct = (nom,prix,id) => {
    return fetch(SERVER_API_URL+'/UpdateProduct.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            nom:nom,
            prix:prix,
            id:id
        })
      })
      .then(response => response.json())
};
export const deleteProduct = (list) => {
    return fetch(SERVER_API_URL+'/DeleteProduct.php',
    {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
        {
            list:list
        })
      })
      .then(response => response.json())
};