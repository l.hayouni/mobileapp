import{
    GET_PRODUCT,
    ADD_PRODUCT,
    EDIT_PRODUCT,
    GET_PRODUCT_SUCCESS,
    GET_COMMANDE_PRODUCT,
    DELETE_PRODUCT
}from './actionsTypes'

export const getProduct=(successCallBack,errorCallBack)=>(
    {
        type:GET_PRODUCT,
        data:{successCallBack,errorCallBack}
    }
)
export const getCommandeProduct=(list,successCallBack,errorCallBack)=>(
    {
        type:GET_COMMANDE_PRODUCT,
        data:{list,successCallBack,errorCallBack}
    }
)
export const setProduct=(response)=>(
    {
        type:GET_PRODUCT_SUCCESS,
        response
    }
)
export const addProduct=(nom,prix,successCallBack,errorCallBack)=>(
    {
        type:ADD_PRODUCT,
        data:{nom,prix,successCallBack,errorCallBack}
    }
)
export const editProduct=(nom,prix,id,successCallBack,errorCallBack)=>(
    {
        type:EDIT_PRODUCT,
        data:{nom,prix,id,successCallBack,errorCallBack}
    }
)
export const deleteProduct=(list,successCallBack,errorCallBack)=>(
    {
        type:DELETE_PRODUCT,
        data:{list,successCallBack,errorCallBack}
    }
)