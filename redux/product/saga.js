import { call, put, takeEvery } from "redux-saga/effects";
import { 
  getProduct,
  newProduct,
  updateProduct,
  deleteProduct,
  getCommandeProduct
} from "./api";
import {
  GET_PRODUCT,
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCESS,
  GET_PRODUCT_SUCCESS,
  ADD_PRODUCT_ERROR,
  GET_COMMANDE_PRODUCT,
  GET_COMMANDE_PRODUCT_SUCCESS,
  EDIT_PRODUCT,
  EDIT_PRODUCT_ERROR,
  EDIT_PRODUCT_SUCCESS,
  DELETE_PRODUCT,
  DELETE_PRODUCT_ERROR,
  DELETE_PRODUCT_SUCCESS
} from "./actionsTypes";

export function* getP({data}) {
    const {successCallBack,errorCallBack}=data
    try {
      const response = yield call(getProduct);
      if (response[0]!=undefined) {
        yield put({ type: GET_PRODUCT_SUCCESS, response});

        successCallBack()
      }
    } catch (error) {
      alert("ERREUR")
    }
  }
export function* getCP({data}) {
    const {list,successCallBack,errorCallBack}=data
    try {
      const response = yield call(getCommandeProduct,list);
      if (response[0]!=undefined) {

        yield put({ type: GET_COMMANDE_PRODUCT_SUCCESS, response});

        successCallBack()
      }
    } catch (error) {
      alert(error)
    }
  }
export function* addP({data}) {
    const {nom,prix,successCallBack,errorCallBack}=data
    try {
      const response = yield call(newProduct,nom,prix);
      if (response==1) {
        yield put({ type: ADD_PRODUCT_SUCCESS});
        successCallBack()
      }
      else{
        yield put({ type: ADD_PRODUCT_ERROR});
        errorCallBack()
      }
    } catch (error) {
      alert('ERREUR')
      
    }
  }
export function* editP({data}) {
    const {nom,prix,id,successCallBack,errorCallBack}=data
    try {
      const response = yield call(updateProduct,nom,prix,id);
      if (response==1) {
        yield put({ type: EDIT_PRODUCT_SUCCESS});
        successCallBack()
      }
      else{
        yield put({ type: EDIT_PRODUCT_ERROR});
        errorCallBack()
      }
    } catch (error) {
      alert('ERREUR')
      
    }
  }
export function* deleteP({data}) {
    const {list,successCallBack,errorCallBack}=data
    try {
      const response = yield call(deleteProduct,list);
      if (response==1) {
        yield put({ type: DELETE_PRODUCT_SUCCESS});
        successCallBack()
      }
      else{
        yield put({ type: DELETE_PRODUCT_ERROR});
        errorCallBack()
      }
    } catch (error) {
      alert('ERREUR')
      
    }
  }
export function* actionWatcherProduct(){
    yield takeEvery(GET_PRODUCT, getP)
    yield takeEvery(GET_COMMANDE_PRODUCT, getCP)
    yield takeEvery(ADD_PRODUCT, addP)
    yield takeEvery(EDIT_PRODUCT, editP)
    yield takeEvery(DELETE_PRODUCT, deleteP)
}