import { all } from "redux-saga/effects";
import { actionWatcherAuth } from "./auth/saga";
import { actionWatcherProduct } from "./product/saga";
import { actionWatcherCommande} from "./commande/saga";
import { actionWatcherChange} from "./change/saga";
import { actionWatcherCharge} from "./charge/saga";
import { actionWatcherNote} from "./note/saga";
import { actionWatcherUsers} from "./user/saga";



export default function* rootSaga() {
  yield all([
    actionWatcherAuth(),
    actionWatcherProduct(),
    actionWatcherCommande(),
    actionWatcherChange(),
    actionWatcherUsers(),
    actionWatcherCharge(),
    actionWatcherNote()
  ]);
}
