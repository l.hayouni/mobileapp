import React,{ Component } from 'react';
import { View ,TouchableOpacity,Alert,BackHandler,ImageBackground} from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Icon, Right,Left,Body,Title,Button ,Footer} from 'native-base';

import { connect } from 'react-redux'
import moment from 'moment';

import style from '../../assets/styles/Home'
import { logout} from "../../redux/auth/actions";
import {COLORS } from "../../util/constants";
import FooterT from "../../Components/FooterT"


class Home extends Component {
  constructor(props){
    super(props);
    this.date= moment()
    .format('YYYY-MM-DD');
      }

  backAction = () => {
    BackHandler.exitApp()
    return true;
  };
    
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.backAction
    );
  }
    
  componentWillUnmount() {
    this.backHandler.remove();
  }    
  logout=()=>{
    Alert.alert("", "Vous voulez se déconnecter?", [
      {
        text: "Non",
        onPress: () => null,
        style: "cancel"
      },
      { text: "Oui", onPress: () => 
        { 
          this.props.logout()
          this.props.navigation.navigate("Login")
         }
    }
    ]);
    
  }
  gotToProduit=()=>{
    this.props.navigation.navigate("Products")
}
  gotToEmployee=()=>{
    this.props.navigation.navigate("Employee")
  }
  gotToClient=()=>{
    let item=this.props.user
    this.props.navigation.navigate("Clients",{item})
  }
  gotToCommande=()=>{
    let item={"id":this.props.user.id}
    this.props.navigation.navigate("Commande",{item})
  }
  gotToCommandeEmployee=()=>{
    let item=this.props.user
    this.props.navigation.navigate("CommandesEmployee",{item})
  }
  goToNote=()=>{
    let item=this.props.user
    this.props.navigation.navigate("Notes",{item})
  }
  goToFacture=()=>{
    this.props.navigation.navigate("Facture")
  }
     render(){
      return (
        <Container>
          <Header style={style.header} androidStatusBarColor={COLORS.primaire}/>
          <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
            <View style={style.loginview}>
              <Text style={style.welcometext}> Frais Trans</Text>
            </View>
            {this.props.user!==null?(this.props.user.role==="admin" ?
            
              <View style={style.buttonview}>
                <Button block style={style.button}  iconLeft onPress={this.gotToCommande} >
                  <Icon style={style.buttontext} active name="md-bookmarks" />
                  <Text  style={style.buttontext}>Commandes</Text>
                </Button>
                <Button block style={style.button}  iconLeft onPress={this.gotToProduit} >
                  <Icon style={style.buttontext} active name="md-basket" />
                  <Text style={style.buttontext}>Produits      </Text>
                </Button>
                <Button block style={style.button}  iconLeft onPress={this.goToNote} >
                  <Icon style={style.buttontext} active name="md-list-box" />
                  <Text style={style.buttontext}>Notes          </Text>
                </Button>
                <Button block style={style.button}  iconLeft onPress={this.gotToEmployee} >
                  <Icon style={style.buttontext} active name="md-person" />
                  <Text style={style.buttontext}>Employées    </Text>
                </Button>
                <Button block style={style.button}  iconLeft onPress={this.gotToClient} >
                  <Icon style={style.buttontext} active name="md-person" />
                  <Text style={style.buttontext}>Clients          </Text>
                </Button>
                <Button block style={style.button}  iconLeft onPress={this.goToFacture} >
                  <Icon style={style.buttontext} active name="md-cash" />
                  <Text style={style.buttontext}>Factures        </Text>
                </Button>
            </View>
            
            :
            <View style={style.buttonview}>
              <Button block style={style.button}  iconLeft onPress={this.gotToCommandeEmployee} >
                  <Icon style={style.buttontext} active name="md-bookmarks" />
                  <Text  style={style.buttontext}>Commandes</Text>
                </Button>
                <Button block style={style.button}  iconLeft onPress={this.gotToClient} >
                  <Icon style={style.buttontext} active name="md-person" />
                  <Text style={style.buttontext}>Clients          </Text>
                </Button>
                <Button block style={style.button}  iconLeft onPress={this.goToNote} >
                  <Icon style={style.buttontext} active name="md-list-box" />
                  <Text style={style.buttontext}>Notes          </Text>
                </Button>
            </View>):null}
            
          </ImageBackground>
          <Footer >
            <FooterT navigation={this.props.navigation} />
          </Footer>
        </Container>
      );
     }
 
}

const mapStateToProps = (state) => {
    return {
      user:state.auth.user,
      produits:state.produit.produits,
      isLoading:state.produit.isLoading
    }
  }
export default connect(mapStateToProps,{ logout})(Home)
