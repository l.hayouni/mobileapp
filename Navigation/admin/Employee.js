import React,{ Component } from 'react';
import {ImageBackground,Alert} from 'react-native';
import { connect } from 'react-redux'
import { ListItem } from 'react-native-elements'
import {
  Container,
  Footer,
  Toast,
  Button,
  Text,
  Icon
} from 'native-base';
import Modal from 'react-native-modal';

import style from '../../assets/styles/Commande'
import { getUsers,deleteUser} from "../../redux/user/actions";
import NewUser from "../../Components/NewUser"
import FooterT from '../../Components/FooterT'
import SelectList from '../../Components/SelectList'
import {COLORS } from "../../util/constants";


class Employee extends Component {
  constructor(props){
    super(props);
    this.total=0
    this.user=this.props.user;
    this.arrayholder = [];
    this.state={
      isModalVisible:false,
      list:[],
    }
      }
  componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }
  componentDidMount() {
    let successCallBack = () => {
      this.arrayholder=this.props.users
      this.setState({list:this.props.users})
    };
    let errorCallBack = () => {
      alert("Erreur")
    };
    this.props.getUsers("employe",successCallBack,errorCallBack)
  }
  searchData(text) {
        const newData = this.arrayholder.filter(item => {
        const itemData = item.email.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1
        });
        
        this.setState({list:newData})
    }
  pressItem=(item)=>{
    this.props.navigation.navigate("DetailEmployee",{item})
  }
  delete=(selectList)=>{
    Alert.alert("", "Etes vous sur de vouloir supprimer?", [
      {
        text: "Non",
        onPress: () => {
            this.props.getUsers("employe",()=>{
              this.arrayholder=this.props.users
              this.setState({list:this.props.users})
            },()=>{});
          },
        style: "cancel"
      },
      {
        text: "Oui",
        onPress: () => {
          let successCallBack = () => {
            this.props.getUsers("employe",()=>{
              this.showToast("Employés bien supprimés !","success")
              this.arrayholder=this.props.users
              this.setState({list:this.props.users})
            },()=>{});
          };
          let errorCallBack=()=>{
              this.showToast("Erreur, Ressayer !","danger")
          }
          this.props.deleteUser(selectList,successCallBack,errorCallBack)
        }
      },
    ])}
    showToast=(msg,type)=>{
      Toast.show({
          text: msg,
          buttonText: "OK",
          type: type,
          duration: 6000,
        })
    }
    render(){
      return (
          <Container>
          <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
              <SelectList
                list={this.state.list}
                arrayholder={this.arrayholder}
                pressItem={this.pressItem}
                delete={this.delete}
                id="id"
                title="Employés"
                searchData={(text)=>this.searchData(text)}
                add={() =>{this.setState({isModalVisible:true})}}
                navigation={this.props.navigation}
                isLoading={(this.props.isLoading)&& this.state.isModalVisible==false}
                renderItem={(item)=>{
                  return(<>
                  <ListItem.Content>
                        <Button  transparent iconLeft >
                            <Icon style={{color:COLORS.primaire}}  active name="md-person" />
                            <Text style={{color:COLORS.primaire}}>{item.email}</Text>
                        </Button>
                  </ListItem.Content>
                  <ListItem.Chevron color="white" />
                  </>)
                }}
              />
              <Modal isVisible={this.state.isModalVisible} style={style.modalP} 
                    animationIn="slideInUp" animationOut="slideOutDown" 
                    onBackdropPress={() =>{
                      this.arrayholder=this.props.users
                      this.setState({isModalVisible:false,list:this.props.users})
                    }
                    }
                    onSwipeComplete={() =>{
                      this.arrayholder=this.props.users
                      this.setState({isModalVisible:false,list:this.props.users})}} swipeDirection="right">
                    <NewUser title="Nouveau Employé" role="employe"/>
              </Modal>
            <Footer >
              <FooterT navigation={this.props.navigation} />
            </Footer>
          </ImageBackground>
          </Container>
      );
     }
 
}

const mapStateToProps = (state) => {
    return {
      user:state.auth.user,
      users:state.user.users,
      isLoading:state.user.isLoading,
    }
  }
export default connect(mapStateToProps,{getUsers,deleteUser})(Employee)
