import React,{ Component } from 'react';
import { ImageBackground} from 'react-native';
import { connect } from 'react-redux'
import moment from 'moment';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import {
  Icon,
  DatePicker,
  Toast ,
  Root,
  Container,
  Header,
  Title,
  Footer,
  Button,
  Left,
  Right,
  Body,
} from 'native-base';


import CommandeList from '../../Components/CommandeList'
import FooterT from '../../Components/FooterT'
import style from '../../assets/styles/Commande'
import { getCommande,verify} from "../../redux/commande/actions";
import { getCommandeProduct} from "../../redux/product/actions";
import {COLORS } from "../../util/constants";



class Commande extends Component {
  constructor(props){
    super(props);
    this.total=0
    this.arrayholder = [];
    this._menu=null,
    this.id=this.props.user.id
    this.role=this.props.user.role
    this.date= moment()
    .format('YYYY-MM-DD');
    this.state={
      isModalVisible:false,
      date:this.date,
      quantite:0,
      list:[],
      showToast: false,
    }
      }
  setMenuRef = ref => {
    this._menu = ref;
  };
  hideMenu = () => {
    this._menu.hide();
  };
 
  showMenu = () => {
    this._menu.show();
  };
  componentDidMount() {
    this.getProduct()
  }
  getProduct=()=>{
    let successCallBack = () => {
      this.arrayholder=this.props.produits
      this.setState({list:this.props.produits})
    };
    let getCommandeSuccessCallBack = (response) => {      
      let list=[];
      response.forEach(item=>list.push(item.id_produit));
      this.props.getCommandeProduct(list,successCallBack,errorCallBack)
    };
    let errorCallBack = () => {
      alert("Erreur")
    };
    this.props.getCommande(getCommandeSuccessCallBack,errorCallBack,this.id,this.state.date);
  }
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state,callback)=>{
        return;
    };
  }
  verify=()=>{
    let successCallBack = (total) => {
      if(total==0)
      Toast.show({
        text: "Commande Valide!",
        buttonText: "OK",
        type: "success",
        duration: 6000,
      })
    else
      Toast.show({
        text: "Commande invalide!",
        buttonText: "OK",
        type: "danger",
        duration: 6000
      })
    };
    this.props.verify(this.state.date,successCallBack)
  }
  
  searchData(text) {
    const newData = this.arrayholder.filter(item => {
    const itemData = item.nom.toUpperCase();
    const textData = text.toUpperCase();
    return itemData.indexOf(textData) > -1
    });
    this.setState({
        list: newData,
      });
}
     render(){
      return (
        <Root>
          <Container>
          <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
            <Header style={style.header} androidStatusBarColor={COLORS.primaire}>
              <Left>
                <Button transparent onPress={()=>this.props.navigation.navigate("Home")}>
                  <Icon name='arrow-back' />
                </Button>
              </Left>
              <Body>
                <Title> {this.state.date} </Title>
              </Body>
              <Right>
                <Button transparent onPress={this.showMenu}>
                  <Icon name='md-more' />
                </Button>
                <Menu ref={this.setMenuRef}>
                  <MenuItem onPress={this.hideMenu}>
                      <DatePicker
                                date={this.state.date}
                                mode="date"
                                placeHolderText="Date"
                                onDateChange={(date) => {
                                  let d=moment(date)
                                  .format('YYYY-MM-DD');
                                  this.setState({date:d})
                                  this.getProduct();
                                }}
                            />
                    </MenuItem>
                    <MenuDivider/>
                    <MenuItem onPress={this.verify}>Vérification</MenuItem>
                </Menu>
              </Right>
            </Header>
            <CommandeList
              list={this.state.list}
              date={this.state.date}
              id={this.id}
              data={this.props.commande}
              type="commande"
              role={this.role}
              isAutorized={true}
              searchData={(text)=>this.searchData(text)}
            />
            
            <Footer >
              <FooterT navigation={this.props.navigation} />
            </Footer>
          </ImageBackground>
          </Container>
          
        </Root>
      );
     }
 
}

const mapStateToProps = (state) => {
    return {
      user:state.auth.user,
      produits:state.produit.commandeProduits,
      isLoading:state.commande.isLoading,
      isLoadingProduct:state.produit.isLoading,
      commande:state.commande.commande
    }
  }
export default connect(mapStateToProps,{getCommande,getCommandeProduct,verify})(Commande)
