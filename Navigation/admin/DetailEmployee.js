import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Text, Icon, Right,Left,Body,Title,Button ,Footer} from 'native-base';
import {ImageBackground} from 'react-native';

import style from '../../assets/styles/Commande'
import {COLORS } from "../../util/constants";
import FooterT from "../../Components/FooterT"

class DetailEmployee extends Component {
    constructor(props){
        super(props);
        this.user=this.props.route.params.item
          }
    componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }
  render() {
    return (
      <Container>
        <Header style={style.header} androidStatusBarColor={COLORS.primaire}>
        <Left>
            <Button transparent onPress={()=>this.props.navigation.navigate("Employee")}>
                <Icon name='arrow-back' />
            </Button>
        </Left>
        <Body>
            <Title>{this.user.email }</Title>
        </Body>
        <Right/>                   
        </Header>
        <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
        <Content >
            <List  >
                    <ListItem style={{borderBottomWidth:0}} />
                    <ListItem  style={{borderBottomWidth:0}} 
                        onPress={()=>
                            {
                            let item=this.user
                            this.props.navigation.navigate("CommandesEmployee",{item})
                            }}>
                        <Left>
                            <Button  transparent iconLeft >
                            <Icon style={{color:COLORS.primaire}} active name="md-bookmarks" />
                            <Text style={{color:COLORS.primaire}}>Commandes</Text>
                            </Button>
                        </Left>
                        <Right>
                            <Icon style={{color:COLORS.primaire}} name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem style={{borderBottomWidth:0}}
                        onPress={()=>
                            {
                            let item=this.user
                            this.props.navigation.navigate("Notes",{item})
                            }}
                    >
                        <Left>
                        <Button transparent iconLeft >
                            <Icon style={{color:COLORS.primaire}}  active name="md-list-box" />
                            <Text style={{color:COLORS.primaire}}>Notes</Text>
                        </Button>
                        </Left>
                        <Right>
                            <Icon style={{color:COLORS.primaire}} name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem style={{borderBottomWidth:0}}
                     onPress={()=>
                        {
                        let item=this.user
                        this.props.navigation.navigate("Clients",{item})
                        }}
                    >
                        <Left>
                        <Button transparent iconLeft >
                            <Icon style={{color:COLORS.primaire}}  active name="md-person" />
                            <Text style={{color:COLORS.primaire}}>Clients</Text>
                        </Button>
                        </Left>
                        <Right>
                            <Icon style={{color:COLORS.primaire}} name="arrow-forward" />
                        </Right>
                    </ListItem>
            </List>
        </Content>
        </ImageBackground>
        <Footer >
            <FooterT navigation={this.props.navigation} />
        </Footer>
      </Container>
    );
  }
}
export default DetailEmployee