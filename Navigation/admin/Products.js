import React,{ Component } from 'react';
import {ImageBackground,Alert} from 'react-native';
import { connect } from 'react-redux'
import { ListItem } from 'react-native-elements'
import {
  Root,
  Container,
  Footer,
  Toast
} from 'native-base';
import Modal from 'react-native-modal';

import style from '../../assets/styles/Commande'
import { getProduct,deleteProduct} from "../../redux/product/actions";
import NewProduct from "../../Components/NewProduct"
import FooterT from '../../Components/FooterT'
import SelectList from '../../Components/SelectList'

class Products extends Component {
  constructor(props){
    super(props);
    this.total=0
    this.arrayholder = [];
    this.state={
      isModalVisible:false,
      list:[],
      item:null
    }
      }
  componentDidMount() {
    let successCallBack = () => {
      this.arrayholder=this.props.produits
      this.setState({list:this.props.produits})
    };
    let errorCallBack = () => {
      alert("Erreur")
    };
    this.props.getProduct(successCallBack,errorCallBack)
  }
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state,callback)=>{
        return;
    };
  }
  searchData(text) {
        const newData = this.arrayholder.filter(item => {
        const itemData = item.nom.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1
        });
        
        this.setState({list:newData})
    }
  pressItem=(item)=>{
    this.setState({
      isModalVisible:true,
      item:item
    })
  }
  delete=(selectList)=>{
    Alert.alert("", "Supprimer les produits?", [
      {
        text: "Non",
        onPress: () => {
          this.props.getProduct(()=>{
            this.arrayholder=this.props.produits
            this.setState({list:this.props.produits})
          })},
        style: "cancel"
      },
      {
        text: "Oui",
        onPress: () => {
          let successCallBack = () => {
            this.props.getProduct(()=>{
              this.showToast("Produits bien supprimés","success")
              this.arrayholder=this.props.produits
              this.setState({list:this.props.produits})
            },()=>{});
          };
          let errorCallBack=()=>{
              this.showToast("Erreur,Ressayer","danger")
          }
          this.props.deleteProduct(selectList,successCallBack,errorCallBack)
        }
      },
    ])}
  showToast=(msg,type)=>{
      Toast.show({
          text: msg,
          buttonText: "OK",
          type: type,
          duration: 6000,
        }) } 
    render(){
      return (
        <Root>
          <Container>
          <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
              <SelectList
                list={this.state.list}
                arrayholder={this.arrayholder}
                pressItem={this.pressItem}
                title='Produits'
                delete={this.delete}
                searchData={(text)=>this.searchData(text)}
                id="id_produit"
                add={() =>{this.setState({isModalVisible:true})}}
                navigation={this.props.navigation}
                isLoading={(this.props.isLoadingProduct||this.props.isLoading)&& this.state.isModalVisible==false}
                renderItem={(item)=>{
                  return(<>
                  <ListItem.Content >
                    <ListItem.Title style={style.listItemTitle}>{item.nom}</ListItem.Title>
                    <ListItem.Subtitle style={style.listItemSubTitle}>Prix : {item.prix} Franc</ListItem.Subtitle>
                  </ListItem.Content>
                  <ListItem.Chevron color="white" />
                  </>)
                }}
              />
              <Modal isVisible={this.state.isModalVisible} style={style.modalP} 
                    animationIn="slideInUp" animationOut="slideOutDown" 
                    onBackdropPress={() =>{
                      this.arrayholder=this.props.produits
                      this.setState({isModalVisible:false,list:this.props.produits,item:null})
                    }
                    }
                    onSwipeComplete={() =>{
                      this.arrayholder=this.props.produits
                      this.setState({isModalVisible:false,list:this.props.produits,item:null})}} swipeDirection="right">
                    <NewProduct item={this.state.item}/>
              </Modal>
            <Footer >
              <FooterT navigation={this.props.navigation} />
            </Footer>
          </ImageBackground>
          </Container>
          
        </Root>
      );
     }
 
}

const mapStateToProps = (state) => {
    return {
      user:state.auth.user,
      produits:state.produit.produits,
      isLoadingProduct:state.produit.isLoading,
    }
  }
export default connect(mapStateToProps,{getProduct,deleteProduct})(Products)
