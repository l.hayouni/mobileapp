import React, { Component } from 'react'
import { View ,ImageBackground,TouchableOpacity,Picker} from 'react-native';
import { connect } from 'react-redux'
import {
    Icon,
    Content,
    Container,
    Header,
    Footer,
    Spinner ,
    Body,
    Button,
    Left,
    Text,
    Title,
    Right,
    List,
    ListItem
  } from 'native-base';
import moment from 'moment';

import { getFacture} from "../../redux/commande/actions";
import FooterT from '../../Components/FooterT'
import style from '../../assets/styles/Commande'
import {COLORS } from "../../util/constants";



class Facture extends Component {
    constructor(props){
        super(props);
        this.state={
          commandes:0,
          charges:0,
          virement:0,
          month:1,
          year:moment().year()
        }
          }
    componentDidMount(){
        this.search()
    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    search=()=>{
        let successCallBack=(data)=>{
            if(data!="0")
              {
                this.setState({
                  commandes:data.totalcmd,
                  charges:data.charges,
                  virement:data.virement,
                })
            }
            else{
                alert("Erreur")
            }
        }
        this.props.getFacture(this.state.month,this.state.year,successCallBack)
    }
    render() {
        return (
                <Container>
                    <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
                        <Header style={style.header} androidStatusBarColor={COLORS.primaire}>
                            <Left>
                                <Button transparent onPress={()=>this.props.navigation.navigate("Home")}>
                                    <Icon name='arrow-back' />
                                </Button>
                            </Left>
                            <Body>
                                <Title>Factures</Title>
                            </Body>
                            <Right/>
                        </Header>
                        <View style={style.searchview}>
                                <Picker style={style.pickerStyle}  
                                            label="Mois"
                                            selectedValue={this.state.month}  
                                            onValueChange={(value) =>  
                                                this.setState({month: value})}  
                                        >  
                                    <Picker.Item label="Janvier" value={1} key={1}/>
                                    <Picker.Item label="Février" value={2} key={2}/>
                                    <Picker.Item label="Mars" value={3} key={3}/>
                                    <Picker.Item label="Avril" value={4} key={4}/>
                                    <Picker.Item label="Mai" value={5} key={5}/>
                                    <Picker.Item label="Juin" value={6} key={6}/>
                                    <Picker.Item label="Juillet" value={7} key={7}/>
                                    <Picker.Item label="Aoùt" value={8} key={8}/>
                                    <Picker.Item label="Septembre" value={9} key={9}/>
                                    <Picker.Item label="Octobre" value={10} key={10}/>
                                    <Picker.Item label="Novembre" value={11} key={11}/>
                                    <Picker.Item label="Décembre" value={12} key={12}/>
                                </Picker> 
                                <Picker style={style.pickerStyle}  
                                            label="Year"
                                            selectedValue={this.state.year}  
                                            onValueChange={(value) =>  
                                                this.setState({year: value})}  
                                        >  
                                    <Picker.Item label={moment().year().toString()} value={moment().year().toString()} key={this.state.year}/>
                                    <Picker.Item label={(moment().year()-1).toString()} value={(moment().year()-1).toString()} key={this.state.year-1}/>
                                </Picker> 
                                <TouchableOpacity style={style.search} onPress={this.search} >
                                    <Icon
                                        name="search"
                                        style={{color:COLORS.primaire}}
                                    />
                                </TouchableOpacity>
                            </View>
                        { (this.props.isLoading)?
                        <Content contentContainerStyle={{ justifyContent: 'center', flex: 1 }}>
                            <Spinner color={COLORS.primaire}/>
                        </Content>:
                        <Content>
                            <List>
                                <ListItem style={style.divider}>
                                    <Body>
                                        <Text style={style.listItemTitle}>Commandes</Text>
                                        <Text style={style.listItemSubTitleBold} note>Total : {this.state.commandes!=null?this.state.commandes:0} Franc</Text>
                                    </Body>
                                </ListItem>
                                <ListItem style={style.divider}>
                                    <Body>
                                        <Text style={style.listItemTitle}>Charges</Text>
                                        <Text style={style.listItemSubTitleBold} note>Total : {this.state.charges!=null?this.state.charges:0} Franc</Text>
                                    </Body>
                                </ListItem>
                                <ListItem style={style.divider}>
                                    <Body>
                                        <Text style={style.listItemTitle}>Virements</Text>
                                        <Text style={style.listItemSubTitleBold} note>Total : {this.state.virement!=null?this.state.virement:0} Franc</Text>
                                    </Body>
                                </ListItem>
                            </List>
                        </Content>}
                        <Footer >
                            <FooterT navigation={this.props.navigation} />
                        </Footer>
                    </ImageBackground>
                </Container>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading:state.commande.isLoading,
      }
  }
export default connect(mapStateToProps,{getFacture})(Facture)