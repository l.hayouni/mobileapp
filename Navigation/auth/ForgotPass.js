import React, { Component } from 'react'
import {View,TextInput,ImageBackground} from 'react-native'
import { Container, Header, Title, Spinner, Button, Left, Right, Body, Icon, Text, Toast,Root} from 'native-base';
import { connect } from 'react-redux'

import style from '../../assets/styles/Login'
import { resetPassword} from "../../redux/auth/actions";
import {COLORS } from "../../util/constants";


class ForgotPass extends Component {
    constructor(props){
        super(props)
        this.username=''
        this.Password=''
    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    changePass= ()=>{
        let successCallBack = () => {
            this.showToast("mot de passe modifié","success")
            this.props.navigation.navigate("Login")
        };
        let errorCallBack=()=>{
            this.showToast("erreur ressayer","danger")
        }
        if(this.username!='' && this.Password!='' )
            this.props.resetPassword(this.username,this.Password,successCallBack,errorCallBack)
        else
            this.showToast("Veuillez remplir tous les champs","danger")

    }
    showToast=(msg,type)=>{
        Toast.show({
            text: msg,
            buttonText: "OK",
            type: type,
            duration: 6000,
          })
    }
    render() {
        return (
        <Root>
            <Container>
                <Header style={style.headerPass} androidStatusBarColor={COLORS.primaire}>
                    <Left>
                        <Button transparent onPress={()=>this.props.navigation.navigate("Login")}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Frais Trans</Title>
                    </Body>
                    <Right/>
                </Header>
                <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
                    <View style={style.loginview}>
                        <Text style={style.textlogin}>Nouveau mot de passe</Text>
                    </View>
                    <View style={style.form}>
                        <TextInput placeholder="Nom d'utilisateur" placeholderTextColor={COLORS.textColor} style={style.textinput} onChangeText={username=>this.username=username}/>
                        <TextInput placeholder="Nouveau mot de passe" placeholderTextColor={COLORS.textColor} secureTextEntry={true} style={style.textinput} onChangeText={password=>this.Password=password}/>
                        { this.props.isLoading ?
                        <Spinner color="#506FA8"/>:
                        <Button rounded block  style={style.button} onPress={this.changePass}>
                            <Text style={style.buttontext}>Modifier</Text>
                        </Button>
                        }
                    </View>
                </ImageBackground>
            </Container>
        </Root>    
        )
    }
}
const mapStateToProps = (state) => {
    return {
       isLoading:state.auth.isLoading
    }
  }
export default connect(mapStateToProps, { resetPassword})(ForgotPass)
