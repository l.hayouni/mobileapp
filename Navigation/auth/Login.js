import React, { Component } from 'react'
import {View,TextInput,ImageBackground,Image,TouchableOpacity} from 'react-native'
import { connect } from 'react-redux'
import {Container, Header, Toast, Spinner, Button, Text,Root} from 'native-base';

import style from '../../assets/styles/Login'
import { loginUser} from "../../redux/auth/actions";
import {COLORS } from "../../util/constants";


class Login extends Component {
    constructor(props){
        super(props)
        this.username=""
        this.Password=""
        this.usernameInput = React.createRef();
        this.passwordInput = React.createRef();
    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    login= ()=>{
        let successCallBack = () => {
            this.refreshLogin()
            this.props.navigation.navigate("Home")
        };
        let errorCallBack=()=>{
            this.showToast("Nom d'utilisateur ou mot de passe incorrrecte","danger")
        }
        this.props.loginUser(this.username,this.Password,successCallBack,errorCallBack)
    }
    showToast=(msg,type)=>{
        Toast.show({
            text: msg,
            buttonText: "OK",
            type: type,
            duration: 6000,
          })
    }
    newpass=()=>{
        this.refreshLogin()
        this.props.navigation.navigate("ForgotPass")
    }
    refreshLogin=()=>{
        this.username=""
        this.Password=""
        this.usernameInput.current.clear();
        this.passwordInput.current.clear();
    }
    render() {
        return (
        <Root>
            <Container>
                <Header style={style.header} androidStatusBarColor={COLORS.primaire}/>
                <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
                    <View style={style.loginview}>
                        <Image source={require('../../assets/images/lock.png')} style={style.imagelogin} />
                        <Text style={style.textlogin}>Frais Trans</Text>
                    </View>
                    <View style={style.form}>
                        <TextInput placeholder="Nom d'utilisateur" placeholderTextColor={COLORS.textColor}  ref={this.usernameInput} style={style.textinput} onChangeText={username=>this.username=username} />
                        <TextInput placeholder="Mot de passe" placeholderTextColor={COLORS.textColor}  ref={this.passwordInput} secureTextEntry={true} style={style.textinput} onChangeText={password=>this.Password=password}/>
                        { this.props.isLoading ?
                        <Spinner color={COLORS.primaire}/>:
                        <Button rounded block  style={style.button} onPress={this.login}>
                            <Text style={style.buttontext}>Connexion</Text>
                        </Button>
                        }
                    </View>
                    <View style={style.forgotpassview}>
                        <TouchableOpacity onPress={this.newpass} >
                            <Text style={style.forgotpasstext}>Mot de passe oublié?</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </Container>
            </Root>    
        )
    }
}
const mapStateToProps = (state) => {
    return {
       isLoading:state.auth.isLoading
    }
  }
export default connect(mapStateToProps, { loginUser})(Login)
