import React,{ Component } from 'react';
import {ImageBackground,Alert} from 'react-native';
import { connect } from 'react-redux'
import { ListItem } from 'react-native-elements'
import {
  Root,
  Container,
  Footer,
  Toast
} from 'native-base';

import style from '../../assets/styles/Commande'
import { getNote,deleteNote} from "../../redux/note/actions";
import FooterT from '../../Components/FooterT'
import SelectList from '../../Components/SelectList'

class Notes extends Component {
  constructor(props){
    super(props);
    this.id=this.props.route.params.item.id
    this.total=0
    this.arrayholder = [];
    this.state={
      list:[],
    }
      }
  componentDidMount() {
    let successCallBack = () => {
      this.arrayholder=this.props.note
      this.setState({list:this.props.note})
    };
    let errorCallBack = () => {
      alert("Erreur")
    };
    this.props.getNote(this.id,successCallBack,errorCallBack)
  }
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state,callback)=>{
        return;
    };
  }
  searchData(text) {
        const newData = this.arrayholder.filter(item => {
        const itemData = item.date.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1
        });
        
        this.setState({list:newData})
    }
  pressItem=(item)=>{
    let callback=(note)=>{
      this.arrayholder=note
      this.setState({list:note})}
    let id=this.id
    this.props.navigation.navigate("NewNote",{item,callback,id})
  }
  delete=(selectList)=>{
    Alert.alert("", "Etes vous sur de vouloir supprimer?", [
      {
        text: "Non",
        onPress: () => {
          this.props.getNote(this.id,()=>{
            this.arrayholder=this.props.note
            this.setState({list:this.props.note})
          })
          },
        style: "cancel"
      },
      {
        text: "Oui",
        onPress: () => {
          let successCallBack = () => {
            this.props.getNote(this.id,()=>{
              this.showToast("Notes bien supprimés","success")
              this.arrayholder=this.props.note
              this.setState({list:this.props.note})
            },()=>{});
          };
          let errorCallBack=()=>{
              this.showToast("Erreur,Ressayer","danger")
          }
          this.props.deleteNote(selectList,successCallBack,errorCallBack)
        }
      },
    ])}
    showToast=(msg,type)=>{
      Toast.show({
          text: msg,
          buttonText: "OK",
          type: type,
          duration: 6000,
        }) } 
    add=()=>{
      let callback=(note)=>{
        this.arrayholder=note
        this.setState({list:note})}
      let id=this.id
      this.props.navigation.navigate("NewNote",{callback,id})
    }
    render(){
      
      return (
        <Root>
          <Container>
          <ImageBackground source={require('../../assets/images/backgradient.jpg')} style={style.imagebackground}>
              <SelectList
                list={this.state.list}
                arrayholder={this.arrayholder}
                pressItem={this.pressItem}
                title='Notes'
                delete={this.delete}
                searchData={(text)=>this.searchData(text)}
                id="id"
                add={this.add}
                navigation={this.props.navigation}
                isLoading={(this.props.isLoading)}
                renderItem={(item)=>{
                  return(<>
                  <ListItem.Content >
                    <ListItem.Title style={style.listItemTitle}>{item.date}</ListItem.Title>
                  </ListItem.Content>
                  <ListItem.Chevron color="white" />
                  </>)
                }}
              />
            <Footer >
              <FooterT navigation={this.props.navigation} />
            </Footer>
          </ImageBackground>
          </Container>
          
        </Root>
      );
     }
 
}

const mapStateToProps = (state) => {
    return {
      user:state.auth.user,
      note:state.note.note,
      isLoading:state.note.isLoading,
    }
  }
export default connect(mapStateToProps,{getNote,deleteNote})(Notes)
