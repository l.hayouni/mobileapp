import React,{ Component } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { connect } from 'react-redux'

import Login from './auth/Login'
import ForgotPass from './auth/ForgotPass'
import Home from './home'
import Commande from './admin/Commande'
import Products from './admin/Products'
import Facture from './admin/Facture'
import Clients from '../Components/Clients'
import CommandesClient from '../Components/CommandesClient'
import CommandesEmployee from '../Components/CommandeEmployee'
import Employee from './admin/Employee'
import DetailEmployee from './admin/DetailEmployee'
import Notes from './Employe/Notes'
import NewNote from '../Components/NewNote'

import Header from "../Components/Header"

const Stack = createStackNavigator();
class Navigation extends Component {
    
     render(){
      return (
          <NavigationContainer>
            <Stack.Navigator>
              <Stack.Screen
                name="Login" 
                component={Login}
                options={({ navigation }) => ({
                  headerShown: false
                })} /> 
              <Stack.Screen 
                name="Home"
                component={Home}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="ForgotPass"
                component={ForgotPass}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                 />
              <Stack.Screen
                name="Commande"
                component={Commande}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="Products"
                component={Products}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="Facture"
                component={Facture}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="Clients"
                component={Clients}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="CommandesClient"
                component={CommandesClient}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="CommandesEmployee"
                component={CommandesEmployee}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="Notes"
                component={Notes}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="NewNote"
                component={NewNote}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="Employee"
                component={Employee}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
              <Stack.Screen
                name="DetailEmployee"
                component={DetailEmployee}
                options={({ navigation }) => ({
                  headerShown: false
                })}
                />
            </Stack.Navigator>
          </NavigationContainer>
      );
     }
 
}
const mapStateToProps = (state) => {
    return {
       // user:state.auth.user
    }
  }
export default connect(mapStateToProps)(Navigation)