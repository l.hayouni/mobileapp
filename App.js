import React,{ Component } from 'react';
import Navigation from './Navigation'
import { Provider } from 'react-redux'
import Store from './redux/store'
import {Root} from 'native-base';
import {View} from 'react-native'
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
        loading: true
      }
  }
  async componentDidMount() {
      await Font.loadAsync({
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
        ...Ionicons.font,
      })
      this.setState({ loading: false })
   } 
     render(){
      if (this.state.loading) {
        return (
          <View></View>
        );
      }
      return (
        <Provider store={Store}>
          <Root>
            <Navigation />
          </Root>
        </Provider>
      );
     }
 
}
export default App