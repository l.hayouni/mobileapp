import { StyleSheet } from 'react-native';
import {COLORS } from "../../util/constants";

export default StyleSheet.create(
    {
        imagebackground:{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'
        },
        header:{
            flex:1,
            backgroundColor:COLORS.primaire
        },
        headerPass:{
            backgroundColor:COLORS.primaire
        },
        loginview:{
            flexGrow:1,
            justifyContent: 'center',
            alignItems: 'center',
        },
        imagelogin:{
            marginTop:50,
        },
        textlogin:{
            color:COLORS.textColor,
            fontSize:16,
            fontWeight:"bold",
            marginTop:10,
            fontFamily: 'monospace'
        },
        form:{
            flexGrow:1,
            justifyContent: 'flex-start',
        },
        textinput:{
            width:300,
            height:50,
            borderBottomColor:COLORS.primaire,
            borderBottomWidth: 1,
            paddingHorizontal:15,
            color:COLORS.textColor,
            marginVertical:10,
            textAlign:'center',
            marginBottom:20
        },
        buttontext:{
            textTransform: 'capitalize'
        },
        button:{
            backgroundColor:COLORS.primaire,
            marginTop:20,
            
        },
        forgotpassview:{
            flexGrow:1,
            justifyContent:'center',
            marginVertical:15,
        },
        forgotpasstext:{
            color:COLORS.textColor,
            fontSize:14,
        },
        loading_container: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 100,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center'
          },
    }
)