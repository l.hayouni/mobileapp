import { StyleSheet } from 'react-native';
import {COLORS } from "../../util/constants";

export default StyleSheet.create(
    {
    imagebackground:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    header:{
      flex:1,
      backgroundColor:COLORS.primaire
    },
    buttontext:{
      textTransform: 'capitalize',
      textAlign:'center'
    },
    button:{
        backgroundColor:COLORS.primaire,
        marginTop:40,
        width:250
        
    },
    welcometext:{
      fontSize:30,
      fontWeight:'bold',
      color:'#506FA8',
      textAlign:'center',
      fontFamily: 'monospace'
    },
    loginview:{
      flexGrow:1,
      justifyContent: 'center',
      alignItems: 'center',
  },
      buttonview:{
        flexGrow:2,
      }
    }
  )