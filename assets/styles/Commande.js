import { StyleSheet } from 'react-native';
import {COLORS } from "../../util/constants";

export default StyleSheet.create(
    {
        imagebackground:{
            flex: 1,
        },
        header:{
          backgroundColor:COLORS.primaire,
        },
        title:{
          fontFamily: 'monospace'
        },
        textArea:{
          borderColor:COLORS.primaire,
          color:COLORS.primaire,
          fontWeight:"bold"
        },
        activeTextStyle:{
          color:COLORS.textColor,
          fontWeight: 'normal'
        },
        contentConatiner:{
          justifyContent: 'center',
          flex: 1
        },
        valideView:{
          flexDirection:"row"
        },
        list:{
        },
        textinput:{
          paddingHorizontal:5
        },
        icon:{
          marginLeft:15
        },
        buttontext:{
          fontSize:13,
          fontWeight:'500',
          color:COLORS.textColor,
          textAlign:'center'
        },
        button:{
            width:155,
            backgroundColor:COLORS.primaire,
            alignSelf:'center',
            paddingVertical:9,
            marginLeft:4
        },
        buttonview:{
          paddingVertical:10,
        },
        addbutton:{
          flexDirection:'row',
          paddingHorizontal:5
        },
        text:{
          color:COLORS.primaire,
          fontSize:15,
          fontWeight:"bold",
          textAlignVertical:"center"
        },
        totalText:{
          color:COLORS.primaire,
          fontSize:15,
          fontWeight:"bold",
          textAlignVertical:"center",
          textAlign:"right",
          paddingHorizontal:20,
          paddingVertical:10,
          borderBottomWidth:1,
          borderBottomColor:COLORS.primaire
        },
        modal:{
          backgroundColor:COLORS.textColor,
          maxHeight:200,
          alignItems: 'center',
        },
        modalP:{
          backgroundColor:COLORS.textColor,
          maxHeight:300,
          alignItems: 'center',
        },
        loading_container: {
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          alignItems: 'center',
          justifyContent: 'center'
        },
        listItem:{
          flex:1,
          flexDirection:"row",
          alignItems:"center"
      },
      searchview:{
        flexDirection:"row",
        height:40,
        marginTop:5,
        marginVertical:5,
        marginTop:10,
        borderColor:COLORS.primaire,
        borderWidth: 1,
        alignItems:'center',
        borderRadius:25,
        marginHorizontal:10,
        backgroundColor:COLORS.textColor
    
      },colview:{
        flexDirection:"row",
        height:50,
        borderBottomColor:COLORS.primaire,
        borderBottomWidth:1,
        paddingHorizontal:20,
        marginVertical:20,
        alignItems:"center",
      },  
      rowview:{
        flexGrow:1,
        flexDirection:"column",
        paddingHorizontal:20,
        
      },
      priceView:{
        flex:2,
        flexDirection:"row",
        justifyContent:"flex-end",
      },
      nomProd:{
        flexGrow:1,
        color:COLORS.primaire,
        fontSize:15,
        fontWeight:"bold",
        textAlignVertical:"center",
        flexWrap: 'wrap'
      },
      priceText:{
        flexGrow:1,
        textAlign:"right",
        color:COLORS.primaire,
        fontSize:15,
        fontWeight:"bold",
        textAlignVertical:"center",
      },
      descriptionText:{
        color:COLORS.textColor,
        fontSize:12,
        textAlignVertical:"center",
        paddingVertical:5
      },
      listItemTitle:{
        color: COLORS.primaire,
        fontWeight: 'bold'
      },
      listItemTitleClient:{
        color:COLORS.primaire,
        fontWeight: 'bold',
        marginLeft:10
      },
      listItemSubTitle:{
        color:COLORS.textColor,
      },
      listItemSubTitleBold:{
        color:COLORS.textColor,
        fontWeight: 'bold',
        fontSize:15
      },
      selected: {
        backgroundColor:COLORS.selectedColor,
        marginLeft: 0,
      },
      normal: {},
      menuItem:{
          flexDirection:'row',
      },
      headerview:{
        flexGrow:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    pickerStyle:{  
      color:COLORS.primaire,
      width:150 ,
      marginLeft:10
  },
    search:{
      justifyContent:"flex-end",
      marginLeft:35
  },
  divider:{
    borderBottomColor:COLORS.primaire
  }
    }
  )