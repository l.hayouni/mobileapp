import { StyleSheet } from 'react-native';
import {COLORS } from "../../util/constants";

export default StyleSheet.create(
    {
        formview:{
            flex:2,
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingHorizontal:20
        },
        title:{
            fontFamily: 'monospace'
        },
        header:{
            backgroundColor:COLORS.primaire
        },
        buttonview:{
            flexGrow:1,
            flexDirection:'row',
            alignItems: 'flex-end',
            justifyContent: 'flex-end'
        },
        buttontext:{
            textTransform: 'capitalize',
            color:COLORS.textColor,
        },
        button:{
            backgroundColor:COLORS.primaire,
            marginTop:20,
        },
        textinput:{
            width:300,
            height:50,
            borderBottomColor:COLORS.primaire,
            borderBottomWidth: 1,
            paddingHorizontal:15,
            color:COLORS.primaire,
            marginVertical:10,
            textAlign:'center',
            marginBottom:20
        },
        headertext:{
            color:COLORS.primaire,
            fontSize:16,
            fontWeight:"bold",
            marginTop:10
        },
        headerview:{
            flexGrow:1,
            justifyContent: 'center',
            alignItems: 'center',
        },
        imagebackground:{
            flex: 1,
        },
      
    }
  )