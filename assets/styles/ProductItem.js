import { StyleSheet } from 'react-native';

export default StyleSheet.create(
    {
        colview:{
            flexDirection:"row",
            height:50,
            borderBottomColor:'#506FA8',
            borderBottomWidth:1,
            paddingHorizontal:20,
            marginVertical:20,
            alignItems:"center",
          },  
          rowview:{
            flexGrow:1,
            flexDirection:"column",
            paddingHorizontal:20,
            
          },
          priceView:{
            flex:2,
            flexDirection:"row",
            justifyContent:"flex-end",
          },
          text:{
            flexGrow:1,
            color:"#506FA8",
            fontSize:15,
            fontWeight:"bold",
            textAlignVertical:"center",
          },
          priceText:{
            flexGrow:1,
            textAlign:"right",
            color:"#506FA8",
            fontSize:15,
            fontWeight:"bold",
            textAlignVertical:"center",
          },
          descriptionText:{
            color:"#fff",
            fontSize:12,
            textAlignVertical:"center",
            paddingVertical:5
          }
    }
  )

  