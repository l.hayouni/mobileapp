import React, { Component } from 'react'
import { Text,ImageBackground} from 'react-native';
import { connect } from 'react-redux'
import {
    Spinner ,
    Button,
    Toast,
    Content,
    Textarea,
    Form,
    Icon,
    Container,
    Header,
    Footer,
    Body,
    Left,
    Title,
    Right,
    Root
  } from 'native-base';
import moment from 'moment';
import style from '../assets/styles/Commande'
import { addNote,getNote,updateNote} from "../redux/note/actions";
import {COLORS } from "../util/constants";
import FooterT from "./FooterT"


class NewNote extends Component {
    constructor(props){
        super(props)
        this.id=this.props.route.params.id
        this.contenu=this.props.route.params.item!==undefined?this.props.route.params.item.contenu:''
        this.id_note=this.props.route.params.item!==undefined?this.props.route.params.item.id:null
        this.date= moment()
        .format('YYYY-MM-DD');
    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    add= ()=>{
        if(this.id_note===null)
        {
            let successCallBack = (id) => {
                this.id_note=id
                this.props.getNote(this.id,()=>{this.showToast("Note bien ajouté","success") },()=>{});
            };
            let errorCallBack=()=>{
                this.showToast("Erreur","danger")
            }
            if(this.contenu!='')
            {
                this.props.addNote(this.contenu,this.date,this.id,successCallBack,errorCallBack)
            }
        }
        else{
            let successCallBack = () => {
                this.props.getNote(this.id,()=>{this.showToast("Note bien modifiée","success") },()=>{});
            };
            let errorCallBack=()=>{
                this.showToast("Erreur","danger")
            }
            if(this.contenu!='')
            {
                this.props.updateNote(this.id_note,this.contenu,successCallBack,errorCallBack)
            }
        }
    }
    showToast=(msg,type)=>{
        Toast.show({
            text: msg,
            buttonText: "OK",
            type: type,
            duration: 6000,
          })
      }
    render() {
        return (
            <Root>
                <Container>
                    <Header style={style.header} androidStatusBarColor={COLORS.primaire}>
                        <Left>
                            <Button  transparent onPress={()=>{
                                this.props.route.params.callback(this.props.note)
                                this.props.navigation.navigate("Notes")}}>
                                <Icon name='arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title>Nouvelle Note</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={this.add}>
                                <Text style={style.buttontext}>Enregistrer</Text>
                            </Button>
                        </Right>    
                    </Header> 
                    <ImageBackground source={require('../assets/images/backgradient.jpg')} style={style.imagebackground}>       
                        <Content padder>    
                            <Form>
                                <Textarea style={style.textArea} rowSpan={15} bordered placeholderTextColor={COLORS.primaire} placeholder="Textarea" defaultValue={this.contenu}  onChangeText={text=>this.contenu=text} />
                            </Form>
                            { this.props.isLoading &&
                            <Spinner color={COLORS.primaire}/>
                            }
                        </Content>
                    </ImageBackground>
                    <Footer >
                        <FooterT navigation={this.props.navigation} />
                    </Footer>
                </Container>        
            </Root>    
        )
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading:state.note.isLoading,
        user:state.auth.user,
        note:state.note.note
    }
  }
export default connect(mapStateToProps,{addNote,getNote,updateNote})(NewNote)
