import React,{ Component } from 'react';
import { View ,TextInput} from 'react-native';
import { ListItem } from 'react-native-elements'
import {
  Icon,
  List,
  Content,
  Header,
  Title,
  Spinner ,
  Button,
  Left,
  Right,
  Body,
} from 'native-base';
import Iconn from 'react-native-vector-icons/MaterialIcons';

import style from '../assets/styles/Commande'
import {COLORS } from "../util/constants";


class SelectList extends Component {
  constructor(props){
    super(props);
    this.state={
      selectList:[],
      selectionMode:false
    }
      }
  componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }   
  toggleSelect = item => {
    this.props.list.map(i => {
      if (item === i) {
        i.selected = !i.selected;
      }
      return i;
    })
    if (this.props.list.filter(i => i.selected).length > 0)
    {
      let tmplist=[]
      this.props.list.filter(i => i.selected).forEach(data=>{
        if(this.props.id==="id")
          tmplist.push(data.id)
        if(this.props.id==="id_produit")
          tmplist.push(data.id_produit)
      })
      this.setState({selectionMode:true,selectList:tmplist})
    }
    else
    {
      this.setState({selectionMode:false,selectList:[]})
    }
  };
  selectAll = () => {
    if(this.props.list.filter(i => i.selected).length===this.state.selectList.filter(i => i.selected).length)
      this.clearSelection()
    else
    {
    this.props.list.map(i => {
        i.selected = true;
      return i;
    })
    this.setState({selectionMode:true,selectList:this.props.list})
  }
  };
  clearSelection = () => {
    this.props.list.map(i => {
      i.selected = false;
      return i;
      })
      this.setState({selectionMode:false,selectList:[]})
    }
  onPress = item => {
    if (this.state.selectionMode) {
      this.toggleSelect(item);
    } else {
      this.props.pressItem(item);
    }
  };
  onLongPress = item => {
    
    if (this.props.list.filter(i => i.selected).length === 0) {
      this.toggleSelect(item)
      this.setState({selectionMode:true})
    }
  };
    render(){
      return (
        <>
          {!this.state.selectionMode?
            <Header style={style.header} androidStatusBarColor={COLORS.primaire}>
              <Left>
                <Button transparent onPress={()=>this.props.navigation.navigate("Home")}>
                  <Icon name='arrow-back' />
                </Button>
              </Left>
              <Body>
                <Title style={style.title}> {this.props.title} </Title>
              </Body>
              <Right>
                <Button transparent onPress={this.props.add}>
                  <Icon name='add' />
                </Button>
              </Right>
            </Header>:
            <Header style={style.header} androidStatusBarColor={COLORS.primaire}>
              <Left>
                <Button transparent onPress={this.clearSelection}>
                  <Icon name='arrow-back' />
                </Button>
              </Left>
              <Body>
                <Title style={style.title}> {this.props.title} </Title>
              </Body>
              <Right>
                <Button transparent onPress={this.selectAll}>
                  <Icon name='md-checkbox-outline' />
                </Button>
                <Button transparent onPress={()=>{
                    this.props.delete(this.state.selectList)
                    this.setState({selectionMode:false})}
                }>
                  <Icon name='trash' />
                </Button>
              </Right>
            </Header>}
            
            { this.props.isLoading?
            <Content contentContainerStyle={style.contentConatiner}>
              <Spinner color={COLORS.primaire}/>
            </Content>:
            <Content>
              <View style={style.buttonview}>
                <View style={style.searchview}>
                    <Iconn
                        name="search"
                        size={25}
                        color={COLORS.primaire}
                        style={style.icon}
                      />
                    <TextInput placeholder="Chercher " placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={text=>this.props.searchData(text)}/>
                </View>
              </View>
              <List>
                  {this.props.list.map(item => {
                    return <ListItem
                    bottomDivider
                    containerStyle={{
                      backgroundColor:'',
                      borderBottomColor:COLORS.primaire
                    }}
                    onPress={() => this.onPress(item)}
                    onLongPress={() => this.onLongPress(item)}
                    key={this.props.id==="id"?item.id:item.id_produit}
                    style={[item.selected ? style.selected : style.normal]}>
                      {this.props.renderItem(item)}
                      </ListItem>
                  })}
              </List>
            </Content>}
          
        </>
      );
     }
 
}

export default SelectList
