
import React,{Component} from 'react'
import { View ,Text} from 'react-native'
import styles from '../assets/styles/ProductItem'

class ProductItem extends Component {
  constructor(props)
  {
    super(props)
    this.commande=this.props.commande!=null?this.props.commande[this.props.commande.findIndex( item => item.id_produit === this.props.produit.id_produit)]:null
  }
  render() {
    return (
        <View style={styles.colview}>
            <Text style={styles.text}>{this.commande!=null?this.commande.quantite+"x":"0x"}</Text>
            <View style={styles.rowview}>
                <Text style={styles.text}>{this.props.produit.nom}</Text>
                <Text style={styles.descriptionText}>{this.props.produit.prix} Franc l'unité</Text>
            </View>
            <Text style={styles.priceText}>{this.props.produit.prix} Franc</Text>
        </View>
      
    )
  }
}

export default ProductItem