import React, { Component } from 'react'
import { Text, View ,TextInput} from 'react-native';
import { connect } from 'react-redux'
import {
    Spinner ,
    Button,
    Toast,
  } from 'native-base';
import style from '../assets/styles/newCommandeProduct'
import { addCharge,getCharge,updateCharge} from "../redux/charge/actions";
import {COLORS } from "../util/constants";


class NewCharge extends Component {
    constructor(props){
        super(props)
        this.total=this.props.item===null?'':this.props.item.total
        this.titre=this.props.item===null?"":this.props.item.titre
    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    add= ()=>{
        if(this.props.item===null)
        {
            let successCallBack = () => {
                this.props.getCharge(this.props.id,this.props.date,()=>{alert("Charge bien ajoutée") },()=>{});
            };
            let errorCallBack=()=>{
                alert("Erreur")
            }
            if(this.total!='' && this.titre!="")
            {
                this.props.addCharge(this.props.id,this.titre,this.total,this.props.date,successCallBack,errorCallBack)
            }
            else{
                alert("Veuillez remplir tout les champs")
            }}
        else{
            let successCallBack = () => {
                this.props.getCharge(this.props.id,this.props.date,()=>{alert("Charge bien modifiée") },()=>{});
            };
            let errorCallBack=()=>{
                alert("Erreur")
            }
            if(this.total!=this.props.item.total || this.titre!=this.props.item.titre)
            {
                this.props.updateCharge(this.props.item.id_charge,this.titre,this.total,successCallBack,errorCallBack)
            }
            else{
                alert("Veuillez remplir tout les champs")
            }
        }
        
    }
    render() {
        const title=this.props.item!=null?"Modifier Charge" : "Nouvelle Charge"
        return (
                <View style={style.formview}>
                    <Text style={style.headertext}>{title}</Text>
                    <TextInput placeholder={this.props.item!=null?this.props.item.titre:"Charge"} placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={text=>this.titre=text}/>
                    <TextInput placeholder={this.props.item!=null?this.props.item.total:"Total"} placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={text=>this.total=text}/>
                    { this.props.isLoading ?
                    <Spinner color={COLORS.primaire}/>
                    : 
                    <Button rounded block style={style.button} onPress={this.add}>
                        <Text style={style.buttontext}>{this.props.item!=null?"Modifier" : "Ajouter"}</Text>
                    </Button>
                    }
                    
                </View>
                
        )
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading:state.charge.isLoading
      }
  }
export default connect(mapStateToProps,{addCharge,getCharge,updateCharge})(NewCharge)
