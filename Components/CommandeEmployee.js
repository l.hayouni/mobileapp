import React,{ Component } from 'react';
import {ImageBackground,Alert} from 'react-native';
import { connect } from 'react-redux'
import moment from 'moment';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import {
  Icon,
  DatePicker,
  Toast ,
  Root,
  Container,
  Header,
  Title,
  Footer,
  Spinner ,
  Button,
  Left,
  Right,
  Body,
  Text,
  Tab,
  Tabs
} from 'native-base';
import Modal from 'react-native-modal';

import style from '../assets/styles/Commande'
import { getCommande,getTotal} from "../redux/commande/actions";
import { getCharge,deleteCharge,getVirement} from "../redux/charge/actions";
import { getCommandeProduct} from "../redux/product/actions";
import FooterT from './FooterT'
import CommandeList from './CommandeList'
import ChargeList from './ChargeList'
import NewCharge from './NewCharge'
import Virement from './Virement'

import {COLORS } from "../util/constants";


class CommandeEmployee extends Component {
  constructor(props){
    super(props);
    this.total=0
    this.id=this.props.route.params.item.id
    this.role=this.props.route.params.item.role
    this._menu=null
    this.arrayholder = [];
    this.chargearrayholder = [];
    this.date= moment()
    .format('YYYY-MM-DD');
    this.isAutorized=true
    this.state={
      list:[],
      chargeList:[],
      date:this.date,
      selectList:[],
      selectionMode:false,
      isModalVisible:false,
      item:null,
      total:null,
      id_charge:null,
    }
      }
  componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }
  toggleSelect = item => {
    this.state.chargeList.map(i => {
      if (item === i) {
        i.selected = !i.selected;
      }
      return i;
    })
    if (this.state.chargeList.filter(i => i.selected).length > 0)
    {
      let tmplist=[]
      this.state.chargeList.filter(i => i.selected).forEach(data=>{
          tmplist.push(data.id_charge)
      })
      this.setState({selectionMode:true,selectList:tmplist})
    }
    else
    {
      this.setState({selectionMode:false,selectList:[]})
    }
  };
    selectAll = () => {
        if(this.state.chargeList.filter(i => i.selected).length===this.state.selectList.filter(i => i.selected).length)
        this.clearSelection()
        else
        {
        this.state.chargeList.map(i => {
            i.selected = true;
        return i;
        })
        this.setState({selectionMode:true,selectList:this.state.chargeList})
    }
    };
    clearSelection = () => {
        this.state.chargeList.map(i => {
        i.selected = false;
        return i;
        })
        this.setState({selectionMode:false,selectList:[]})
        }
    onPress = item => {
        if (this.state.selectionMode) {
        this.toggleSelect(item);
        } else {
        this.pressItem(item);
        }
    };
    pressItem=(item)=>{
        this.setState({
            item:item,
            isModalVisible:true
        })
    }
    onLongPress = item => {
        
        if (this.isAutorized&&this.state.chargeList.filter(i => i.selected).length === 0) {
        this.toggleSelect(item)
        this.setState({selectionMode:true})
        }
    };
    setMenuRef = ref => {
        this._menu = ref;
    };
    hideMenu = () => {
        this._menu.hide();
    };
 
    showMenu = () => {
        this._menu.show();
    };
    getVirement=()=>{
      let successCallBack = (response) => {
          this.setState({
              total:response.total,
              id_charge:response.id_charge})
        };
      let errorCallBack=()=>{
        this.setState({
          total:null,
          id_charge:null})
    };
      this.props.getVirement(this.id,this.state.date,successCallBack,errorCallBack);
  }
    componentDidMount() {
      this.getProduct()
      this.props.getCharge(this.id,this.state.date,this.getChargeSuccessCallBack);
      this.getVirement()
    }
    getProduct=()=>{
      let successCallBack = () => {
        this.arrayholder=this.props.produits
        this.setState({list:this.props.produits})
      };
      let getCommandeSuccessCallBack = (response) => {      
        let list=[];
        
        response.forEach(item=>list.push(item.id_produit));
        this.props.getCommandeProduct(list,successCallBack,errorCallBack)
      };
      let errorCallBack = () => {
        alert("Erreur")
      };
      this.props.getCommande(getCommandeSuccessCallBack,errorCallBack,this.id,this.state.date);
    }
    getChargeSuccessCallBack = () => {
        this.chargearrayholder=this.props.charge
        this.setState({chargeList:this.props.charge})
      };
    componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }
    searchData(text) {
      const newData = this.arrayholder.filter(item => {
      const itemData = item.nom.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1
      });
      this.setState({
          list: newData,
        });
  }
  searchCharge(text) {
    const newData = this.chargearrayholder.filter(item => {
    const itemData = item.titre.toUpperCase();
    const textData = text.toUpperCase();
    return itemData.indexOf(textData) > -1
    });
    this.setState({
        chargeList: newData,
      });
}
  delete=()=>{
    Alert.alert("", "Supprimer les charges?", [
      {
        text: "Non",
        onPress: () => {
            this.props.getCharge(this.id,this.state.date,()=>{
              this.chargearrayholder=this.props.charge
              this.setState({chargeList:this.props.charge})
            })
        },
        style: "cancel"
      },
      {
        text: "Oui",
        onPress: () => {
          let successCallBack = () => {
            this.props.getCharge(this.id,this.state.date,()=>{
              this.showToast("Charges bien supprimés","success")
              this.chargearrayholder=this.props.charge
              this.setState({chargeList:this.props.charge})
            },()=>{});
          };
          let errorCallBack=()=>{
              this.showToast("Erreur,Ressayer","danger")
          }
          this.props.deleteCharge(this.state.selectList,this.state.date,this.id,successCallBack,errorCallBack)
        }
      },
    ])}
  showToast=(msg,type)=>{
      Toast.show({
          text: msg,
          buttonText: "OK",
          type: type,
          duration: 6000,
        }) }  
    render(){
      this.isAutorized=this.props.user.role==='admin'||this.state.date===this.date||(this.props.commande.length===0&&this.props.charge.length===0&&this.state.total===null)
      return (
        <Root>
          <Container>
          <ImageBackground source={require('../assets/images/backgradient.jpg')} style={style.imagebackground}>
          {this.state.selectionMode&&this.isAutorized?
            <Header style={style.header} androidStatusBarColor={COLORS.primaire}>
            <Left>
              <Button transparent onPress={this.clearSelection}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
              <Title style={style.title}></Title>
            </Body>
            <Right>
              <Button transparent onPress={this.selectAll}>
                <Icon name='md-checkbox-outline' />
              </Button>
              <Button transparent onPress={()=>{
                  this.delete()
                  this.setState({selectionMode:false})}
              }>
                <Icon name='trash' />
              </Button>
            </Right>
          </Header>:
          <Header transparent  style={style.header} androidStatusBarColor={COLORS.primaire}>
            <Left>
              <Button transparent onPress={()=>this.props.navigation.navigate("Home")}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
              <Title style={style.title}>{this.state.date}</Title>
            </Body>
            <Right>
                  <Button transparent onPress={this.showMenu}>
                      <Icon name='md-more' />
                  </Button>
                  <Menu ref={this.setMenuRef}>
                  <MenuItem onPress={this.hideMenu}>
                  <Button  transparent iconLeft>
                      <Icon name="calendar" style={{color:"#000"}}/>
                      <DatePicker
                                  date={this.state.date}
                                  mode="date"
                                  placeHolderText="Date"
                                  onDateChange={(date) => {
                                  let d=moment(date)
                                  .format('YYYY-MM-DD');
                                  this.setState({date:d})
                                  this.getProduct();
                                  this.props.getCharge(this.id,this.state.date,this.getChargeSuccessCallBack);
                                  this.getVirement()
                                  
                                }}
                              />
                  </Button>
                  </MenuItem>
                  <MenuDivider/>
                  
                  {this.isAutorized&&
                  <MenuItem>
                      <Button  transparent iconLeft onPress={()=>{this.setState({isModalVisible:true})}}>
                          <Icon name="add" style={{color:"#000"}}/>
                          <Text style={{color:"#000",textTransform:"capitalize",fontWeight:"normal"}}>Nouvelle Charge</Text>
                      </Button>
                  </MenuItem>}
                  <MenuItem>
                      <Button  transparent iconLeft onPress={()=>{this.props.getTotal(this.state.date,this.id)}}>
                          <Icon name="md-calculator" style={{color:"#000"}}/>
                          <Text style={{color:"#000",textTransform:"capitalize",fontWeight:"normal"}}>Total</Text>
                      </Button>
                  </MenuItem>
                  </Menu>
              </Right>
            </Header>
          }
            <Tabs >
                <Tab style={{backgroundColor:''}}  tabStyle={style.header} textStyle={{color:COLORS.textColor}} activeTabStyle={style.header} activeTextStyle={style.activeTextStyle} heading="Produits">
                  <CommandeList
                    list={this.state.list}
                    date={this.state.date}
                    id={this.id}
                    data={this.props.commande}
                    type="commande"
                    role={this.role}
                    isAutorized={this.isAutorized}
                    searchData={(text)=>this.searchData(text)}
                  />
                </Tab>
                <Tab style={{backgroundColor:''}} tabStyle={style.header} textStyle={{color:COLORS.textColor}} activeTabStyle={style.header} activeTextStyle={style.activeTextStyle} heading="Charges">
                    <>
                    <ChargeList
                        list={this.state.chargeList}
                        isLoading={this.props.isLoadingCharge}
                        onPress={(item)=>{this.onPress(item)}}
                        onLongPress={(item)=>{this.onLongPress(item)}}
                        searchData={(text)=>this.searchCharge(text)}
                    />
                    <Modal isVisible={this.state.isModalVisible&&(this.props.user.role==='admin'||this.isAutorized===true)} style={style.modalP} 
                    animationIn="slideInUp" animationOut="slideOutDown" 
                    onBackdropPress={() =>{
                      this.chargearrayholder=this.props.charge
                      this.setState({isModalVisible:false,chargeList:this.props.charge,item:null})
                    }
                    }
                    onSwipeComplete={() =>{
                      this.chargearrayholder=this.props.charge
                      this.setState({isModalVisible:false,chargeList:this.props.charge,item:null})}} swipeDirection="right">
                        <NewCharge id={this.id} date={this.state.date} item={this.state.item}/>
                    </Modal>
                    </>
                </Tab>
                <Tab style={{backgroundColor:''}} tabStyle={style.header} textStyle={{color:COLORS.textColor}} activeTabStyle={style.header} activeTextStyle={style.activeTextStyle} heading="Virement">
                   <Virement id={this.id} date={this.state.date} total={this.state.total} id_charge={this.state.id_charge} getVirement={this.getVirement} isAutorized={this.isAutorized} role={this.role}/>
                </Tab>
            </Tabs>
            <Footer >
              <FooterT navigation={this.props.navigation} />
            </Footer>
          </ImageBackground>
          </Container>
        </Root>  
      );
     }
 
}

const mapStateToProps = (state) => {
  return {
    user:state.auth.user,
    produits:state.produit.commandeProduits,
    isLoadingCommande:state.commande.isLoading,
    isLoadingProduct:state.produit.isLoading,
    commande:state.commande.commande,
    charge:state.charge.charge,
    isLoadingCharge:state.charge.isLoading,
  }
}
export default connect(mapStateToProps,{getCommande,getTotal,getCommandeProduct,getCharge,deleteCharge,getVirement})(CommandeEmployee)
