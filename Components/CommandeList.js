import React,{ Component } from 'react';
import { View ,TouchableOpacity,TextInput} from 'react-native';
import { connect } from 'react-redux'

import Modal from 'react-native-modal';
import Iconn from 'react-native-vector-icons/MaterialIcons';
import {
  List,
  Content,
  Spinner ,
  Text
} from 'native-base';


import NewCommandeProduct from './NewCommandeProduct'
import style from '../assets/styles/Commande'
import {COLORS } from "../util/constants";



class CommandeList extends Component {
  constructor(props){
    super(props);
    this.total=0
    this.state={
        isModalVisible:false,
      }
      }
    componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }
     render(){
      this.total=0
      if(this.props.data!=null)
            this.props.data.forEach(data=>{
              this.total+=parseFloat(data.total)
            })
      return (
        <>
            { (this.props.isLoadingProduct||this.props.isLoadingCommande||this.props.isLoadingChange)&& this.state.isModalVisible==false?
            <Content contentContainerStyle={{ justifyContent: 'center', flex: 1 }}>
              <Spinner color={COLORS.primaire}/>
            </Content>:
            <Content>
                <View style={style.buttonview}>
                  <View style={style.searchview}>
                      <Iconn
                          name="search"
                          size={25}
                          color={COLORS.primaire}
                          style={style.icon}
                        />
                      <TextInput placeholder="Chercher " placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={text=>this.props.searchData(text)}/>
                  </View>
                </View>
                <Text style={style.totalText}>Total : {this.total} Franc</Text>
                <List >
                    {this.props.list.map(item => {
                      return <View key={item.id_produit} style={style.listItem}>
                      <TouchableOpacity  style={{flex:2}}  onPress={() =>{this.setState({isModalVisible:true,item:item})}}>
                        <View style={style.colview}>
                            <Text style={style.text}>{this.props.data!=null&&this.props.data[this.props.data.findIndex( data => data.id_produit === item.id_produit)]!=undefined?this.props.data[this.props.data.findIndex( data => data.id_produit === item.id_produit)].quantite+"x":"0x"}</Text>
                            <View style={style.rowview}>
                              <Text style={style.nomProd}>{item.nom}</Text>
                              <Text style={style.descriptionText}>{item.prix} Franc l'unité</Text>
                            </View>
                            <Text style={style.priceText}>{this.props.data!=null&&this.props.data[this.props.data.findIndex( data => data.id_produit === item.id_produit)]!=undefined?this.props.data[this.props.data
                              .findIndex( data => data.id_produit === item.id_produit)].total:0} Franc</Text>
                        </View>
                      </TouchableOpacity>
                      </View>
                    })}
                </List>  
                         
                <Modal isVisible={this.state.isModalVisible&&(this.props.user.role==='admin'||this.props.isAutorized===true)} style={style.modal} 
                      animationIn="slideInUp" animationOut="slideOutDown" 
                      onBackdropPress={() =>{
                        this.setState({isModalVisible:false})}}
                      onSwipeComplete={() =>{
                        this.setState({isModalVisible:false})}} swipeDirection="right">
                      <NewCommandeProduct type={this.props.type} id={this.props.id} produit={this.state.item} date={this.props.date}/>
                </Modal>    
            </Content>}
        </>
      );
     }
 
}


const mapStateToProps = (state) => {
  return {
    user:state.auth.user,
    isLoadingCommande:state.commande.isLoading,
    isLoadingProduct:state.produit.isLoading,
    isLoadingChange:state.change.isLoading,
  }
}
export default connect(mapStateToProps)(CommandeList)
