import React,{ Component } from 'react';
import {ImageBackground,Alert} from 'react-native';
import { connect } from 'react-redux'
import moment from 'moment';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import {
  Icon,
  DatePicker,
  Subtitle ,
  Root,
  Container,
  Header,
  Title,
  Footer,
  Spinner ,
  Button,
  Left,
  Right,
  Body,
  Text,
  Tab,
  Tabs
} from 'native-base';


import style from '../assets/styles/Commande'
import { getCommande} from "../redux/commande/actions";
import { getChange} from "../redux/change/actions";
import { getCommandeProduct} from "../redux/product/actions";
import FooterT from './FooterT'
import CommandeList from './CommandeList'
import {COLORS } from "../util/constants";


class CommandesClient extends Component {
  constructor(props){
    super(props);
    this.total=0
    this.id=this.props.route.params.item.id
    this.nom=this.props.route.params.item.email
    this.role=this.props.route.params.item.role
    this._menu=null
    this.arrayholderProduit = [];
    this.arrayholderChange=[];
    this.isAutorized=true;
    this.date= moment()
    .format('YYYY-MM-DD');
    this.state={
      isModalVisible:false,
      listChange:[],
      listProduit:[],
      date:this.date
    }
      }
    setMenuRef = ref => {
        this._menu = ref;
    };
    hideMenu = () => {
        this._menu.hide();
    };
 
    showMenu = () => {
        this._menu.show();
    };
    componentDidMount() {
      this.getProduct()
    }
    getProduct=()=>{
      let successCallBackProduit = () => {
        this.arrayholderProduit=this.props.produits
        this.setState({listProduit:this.props.produits})
      };
      let successCallBackChange = () => {
        this.arrayholderChange=this.props.produits
        this.setState({listChange:this.props.produits})
      };
      let getCommandeSuccessCallBack = (response) => {      
        let list=[];
        
        response.forEach(item=>list.push(item.id_produit));
        this.props.getCommandeProduct(list,successCallBackProduit,errorCallBack)
      };
      let getChangeSuccessCallBack = (response) => {      
        let list=[];
        
        response.forEach(item=>list.push(item.id_produit));
        this.props.getCommandeProduct(list,successCallBackChange,errorCallBack)
      };
      let errorCallBack = () => {
        alert("Erreur")
      };
      this.props.getCommande(getCommandeSuccessCallBack,errorCallBack,this.id,this.state.date);
      this.props.getChange(getChangeSuccessCallBack,errorCallBack,this.id,this.state.date);
    }
    componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }
    searchProductData(text) {
      const newData = this.arrayholderProduit.filter(item => {
      const itemData = item.nom.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1
      });
      this.setState({
          listProduit: newData,
        });
  }
    searchChangeData(text) {
      const newData = this.arrayholderChange.filter(item => {
      const itemData = item.nom.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1
      });
      this.setState({
          listChange: newData,
        });
  }
    
    render(){
      this.isAutorized=this.state.date===this.date||(this.props.commande.length===0&&this.props.change.length===0)
      return (
          <Container>
          <ImageBackground source={require('../assets/images/backgradient.jpg')} style={style.imagebackground}>
          <Header transparent  style={style.header} androidStatusBarColor={COLORS.primaire}>
              <Left>
                <Button transparent onPress={()=>this.props.navigation.navigate("Clients")}>
                  <Icon name='arrow-back' />
                </Button>
              </Left>
              <Body>
                <Title style={style.title}>{this.nom}</Title>
                <Subtitle>{this.state.date}</Subtitle>
              </Body>
              <Right>
                    <Button transparent onPress={this.showMenu}>
                        <Icon name='md-more' />
                    </Button>
                    <Menu ref={this.setMenuRef}>
                    <MenuItem onPress={this.hideMenu}>
                        <DatePicker
                                    date={this.state.date}
                                    mode="date"
                                    placeHolderText="Date"
                                    onDateChange={(date) => {
                                    let d=moment(date)
                                    .format('YYYY-MM-DD');
                                    this.setState({date:d})
                                    this.getProduct()

                                    }}
                                />
                        </MenuItem>
                    </Menu>
                </Right>
                
            </Header> 
            <Tabs >
                <Tab style={{backgroundColor:''}}  tabStyle={style.header} textStyle={{color:COLORS.textColor}} activeTabStyle={style.header} activeTextStyle={style.activeTextStyle} heading="Commande">
                  <CommandeList
                    list={this.state.listProduit}
                    date={this.state.date}
                    id={this.id}
                    data={this.props.commande}
                    type="commande"
                    role={this.role}
                    isAutorized={this.isAutorized}
                    searchData={(text)=>this.searchProductData(text)}
                  />
                </Tab>
                <Tab style={{backgroundColor:''}} tabStyle={style.header} textStyle={{color:COLORS.textColor}} activeTabStyle={style.header} activeTextStyle={style.activeTextStyle} heading="Change">
                      <CommandeList
                      list={this.state.listChange}
                      date={this.state.date}
                      id={this.id}
                      data={this.props.change}
                      type="change"
                      role={this.role}
                      isAutorized={this.isAutorized}
                      searchData={(text)=>this.searchChangeData(text)}
                    />
                </Tab>
            </Tabs>
            <Footer >
              <FooterT navigation={this.props.navigation} />
            </Footer>
          </ImageBackground>
          </Container>
          
      );
     }
 
}

const mapStateToProps = (state) => {
  return {
    user:state.auth.user,
    produits:state.produit.commandeProduits,
    isLoadingCommande:state.commande.isLoading,
    isLoadingProduct:state.produit.isLoading,
    commande:state.commande.commande,
    change:state.change.change,
    isLoadingChange:state.change.isLoading,
  }
}
export default connect(mapStateToProps,{getCommande,getCommandeProduct,getChange})(CommandesClient )
