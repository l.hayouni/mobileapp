import React, { Component } from 'react'
import {  View ,TextInput,Image} from 'react-native';
import { connect } from 'react-redux'
import {
    Root,
    Content,
    Spinner ,
    Button,
    Text,
    Toast,
  } from 'native-base';
import style from '../assets/styles/newCommandeProduct'
import { addCharge,updateCharge} from "../redux/charge/actions";
import {COLORS } from "../util/constants";


class Virement extends Component {
    constructor(props){
        super(props)
        this.total=this.props.total
        this.id_charge=this.props.id_charge
    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    edit= ()=>{
        if(this.props.user.role==='admin'||this.props.isAutorized)
        {
        if(this.props.total===null)
        {
            let successCallBack = () => {
                this.props.getVirement()
                this.showToast("Virement Bien ajouté","success")
              };
            let errorCallBack=()=>{
                this.showToast("Erreur","danger")
            }
            this.props.addCharge(this.props.id,'Virement',this.total,this.props.date,successCallBack,errorCallBack)
        }
        else{
            let successCallBack = () => {
                this.props.getVirement()
                this.showToast("Virement Bien modifié","success")
              };
            let errorCallBack=()=>{
            this.showToast("Erreur","danger")
        }
            this.props.updateCharge(this.id_charge,'Virement',this.total,successCallBack,errorCallBack)
        }}
        
    }
    showToast=(msg,type)=>{
        Toast.show({
            text: msg,
            buttonText: "OK",
            type: type,
            duration: 6000,
          }) } 
    render() {
        let total=this.props.total
        return (
                    <Root>  
                        <Content contentContainerStyle={{ justifyContent: 'center', flexGrow: 1 }}>
                            <View style={style.headerview}>
                                <Image source={require('../assets/images/add.png')} />
                                <Text style={style.headertext}>Virement</Text>
                            </View>
                            <View style={style.formview}>
                                <TextInput placeholder='montant' placeholderTextColor="#fff" defaultValue={total} placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={text=>this.total=text}/>
                                { this.props.isLoading ?
                                <Spinner color={COLORS.primaire}/>
                                : 
                                <Button rounded block style={style.button} onPress={this.edit}>
                                    <Text style={style.buttontext}>Valider</Text>
                                </Button>
                                }
                                
                            </View>
                        </Content>
                    </Root> 
        )
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading:state.charge.isLoading,
        user:state.auth.user
      }
  }
export default connect(mapStateToProps,{addCharge,updateCharge})(Virement)
