import React,{ Component } from 'react';
import { Text, View ,TouchableOpacity,Alert} from 'react-native';
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/AntDesign';
import { logout} from "../redux/auth/actions";
class Header extends Component {
    logout=()=>{
        Alert.alert("", "Vous voulez se déconnecter?", [
          {
            text: "Non",
            onPress: () => null,
            style: "cancel"
          },
          { text: "Oui", onPress: () => 
            { 
              this.props.logout()
              this.props.navigate()
             }
        }
        ]);
        
      }
    render() {
        return (
            <View style={{marginRight:15,flexDirection:'row'}}  >
                {this.props.user!=null?
                    <Text style={{fontSize:15,color:'white'}}>{this.props.user.email} , </Text>
                    :null}
                
                <TouchableOpacity style={{flexDirection:'row'}}  onPress={this.logout}>
                    <Text style={{marginRight:5,fontSize:15,color:'white'}}> Déconnexion</Text>
                    <Icon
                    name="logout"
                    size={20}
                    color='white'
                    />
                </TouchableOpacity>
            
            </View>
        )
    }
}
const mapStateToProps = (state) => {
    return {
      user:state.auth.user,
    }
  }
export default connect(mapStateToProps,{ logout})(Header)