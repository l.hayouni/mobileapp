import React,{ Component } from 'react';
import { View ,TextInput,Text} from 'react-native';
import { ListItem } from 'react-native-elements'
import {
  Icon,
  List,
  Content,
  Header,
  Title,
  Spinner ,
  Button,
  Left,
  Right,
  Body,
} from 'native-base';
import Iconn from 'react-native-vector-icons/MaterialIcons';

import style from '../assets/styles/Commande'
import {COLORS } from "../util/constants";


class ChargeList extends Component {
  constructor(props){
    super(props);
    this.state={
      selectList:[],
      selectionMode:false
    }
      } 
  componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }
    render(){
      this.total=0
      if(this.props.list!=null)
            this.props.list.forEach(data=>{
              this.total+=parseFloat(data.total)
            })
      return (
        <>
            { this.props.isLoading?
            <Content contentContainerStyle={style.contentConatiner}>
              <Spinner color={COLORS.primaire}/>
            </Content>:
            <Content>
              <View style={style.buttonview}>
                <View style={style.searchview}>
                    <Iconn
                        name="search"
                        size={25}
                        color={COLORS.primaire}
                        style={style.icon}
                      />
                    <TextInput placeholder="Chercher " placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={text=>this.props.searchData(text)}/>
                </View>
              </View>
              <Text style={style.totalText}>Total : {this.total} Franc</Text>
              <List>
                  {this.props.list.map(item => {
                    return <ListItem
                    bottomDivider
                    containerStyle={{
                      backgroundColor:'',
                      borderBottomColor:COLORS.primaire
                    }}
                    onPress={() => this.props.onPress(item)}
                    onLongPress={() => this.props.onLongPress(item)}
                    key={item.id_charge}
                    style={[item.selected ? style.selected : style.normal]}>
                      <ListItem.Content >
                        <ListItem.Title style={style.listItemTitle}>{item.titre}</ListItem.Title>
                        <ListItem.Subtitle style={style.listItemSubTitle}>Total : {item.total} Franc</ListItem.Subtitle>
                      </ListItem.Content>
                    </ListItem>
                  })}
              </List>
            </Content>}
          
        </>
      );
     }
 
}

export default ChargeList
