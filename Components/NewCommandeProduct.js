import React, { Component } from 'react'
import { Text, View ,TextInput ,TouchableOpacity,ActivityIndicator} from 'react-native';
import { connect } from 'react-redux'
import {
    Spinner ,
    Button,
  } from 'native-base';
import style from '../assets/styles/newCommandeProduct'
import { addCommandeProduct,getCommande,deleteCommandeProduct} from "../redux/commande/actions";
import { addChangeProduct,getChange,deleteChangeProduct} from "../redux/change/actions";
import {COLORS } from "../util/constants";



class NewCommandeProduct extends Component {
    constructor(props){
        super(props)
        this.quantite=''
    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    add= ()=>{
        if(this.props.type==='commande')
        {
            let successCallBack = () => {
                let getCommandeSuccessCallBack = (response) => {      
                  };
                  let errorCallBack = () => {
                    alert("Erreur")
                  };
                this.props.getCommande(getCommandeSuccessCallBack,errorCallBack,this.props.id,this.props.date);
                alert("Produit bien ajouté")
            };
            let errorCallBack=()=>{
                alert("Erreur")
            }
            if(this.quantite!='' && this.quantite!=0)
            {
                this.props.addCommandeProduct(this.props.id,this.props.produit.id_produit,this.quantite,this.props.date,successCallBack,errorCallBack)
            }
            if(this.quantite==0){
                this.props.deleteCommandeProduct(this.props.id,this.props.produit.id_produit,this.props.date,successCallBack,errorCallBack)
            }
        }
        else{
            let successCallBack = () => {
                let getCommandeSuccessCallBack = (response) => {      
                };
                let errorCallBack = () => {
                  alert("Erreur")
                };
                this.props.getChange(getCommandeSuccessCallBack,errorCallBack,this.props.id,this.props.date);
                alert("Produit bien ajouté")

            };
            let errorCallBack=()=>{
                alert("Erreur")
            }
            if(this.quantite!='' && this.quantite!=0)
            {
                this.props.addChangeProduct(this.props.id,this.props.produit.id_produit,this.quantite,this.props.date,successCallBack,errorCallBack)
            }
            if(this.quantite==0){
                this.props.deleteChangeProduct(this.props.id,this.props.produit.id_produit,this.props.date,successCallBack,errorCallBack)
            }
        }
        
    }
    render() {
        return (
                <View style={style.formview}>
                    <Text style={style.headertext}>{this.props.produit.nom}</Text>
                    <TextInput placeholder="Quantite" placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={quantite=>this.quantite=quantite}/>
                    { this.props.isLoadingChange||this.props.isLoadingCommande ?
                    <Spinner color={COLORS.primaire}/>
                    : 
                    <Button rounded block style={style.button} onPress={this.add}>
                        <Text style={style.buttontext}>Valider</Text>
                    </Button>
                    }
                    
                </View>
                
        )
    }
}
const mapStateToProps = (state) => {
    return {
        user:state.auth.user,
        produits:state.produit.produits,
        isLoadingCommande:state.commande.isLoading,
        isLoadingChange:state.change.isLoading
      }
  }
export default connect(mapStateToProps,{addCommandeProduct,getCommande,deleteCommandeProduct,addChangeProduct,getChange,deleteChangeProduct})(NewCommandeProduct)
