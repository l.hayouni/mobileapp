import React,{ Component } from 'react';
import {ImageBackground,Alert} from 'react-native';
import { connect } from 'react-redux'
import { ListItem } from 'react-native-elements'
import {
  Container,
  Footer,
  Toast,
  Button,
  Text,
  Icon,Root
} from 'native-base';
import Modal from 'react-native-modal';

import style from '../assets/styles/Commande'
import { getClients,deleteClient} from "../redux/user/actions";
import NewUser from "./NewUser"
import FooterT from './FooterT'
import SelectList from './SelectList'
import {COLORS } from "../util/constants";

class Clients extends Component {
  constructor(props){
    super(props);
    this.total=0
    this.user=this.props.route.params.item;
    this.arrayholder = [];
    this.state={
      isModalVisible:false,
      list:[],
    }
      }
  componentWillUnmount() {
      // fix Warning: Can't perform a React state update on an unmounted component
      this.setState = (state,callback)=>{
          return;
      };
    }
  componentDidMount() {
    let successCallBack = () => {
      this.arrayholder=this.props.clients
      this.setState({list:this.props.clients})
    };
    let errorCallBack = () => {
      alert("Erreur")
    };
    this.props.getClients(this.user.id,successCallBack,errorCallBack)
  }
  searchData(text) {
        const newData = this.arrayholder.filter(item => {
        const itemData = item.email.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1
        });
        
        this.setState({list:newData})
    }
  pressItem=(item)=>{
    this.props.navigation.navigate("CommandesClient",{item})
  }
  delete=(selectList)=>{
    Alert.alert("", "Supprimer les clients?", [
      {
        text: "Non",
        onPress: () => null,
        style: "cancel"
      },
      {
        text: "Oui",
        onPress: () => {
          let successCallBack = () => {
            this.props.getClients(this.user.id,()=>{
              this.showToast("Clients bien supprimés !","success")
              this.arrayholder=this.props.clients
              this.setState({list:this.props.clients})
            },()=>{});
          };
          let errorCallBack=()=>{
              this.showToast("Erreur, Ressayer !","danger")
          }
          this.props.deleteClient(selectList,successCallBack,errorCallBack)
        }
      },
    ])}
    showToast=(msg,type)=>{
      Toast.show({
          text: msg,
          buttonText: "OK",
          type: type,
          duration: 6000,
        })
    }
    render(){
      return (
        <Root>
          <Container>
          <ImageBackground source={require('../assets/images/backgradient.jpg')} style={style.imagebackground}>
              <SelectList
                list={this.state.list}
                arrayholder={this.arrayholder}
                pressItem={this.pressItem}
                delete={this.delete}
                id="id"
                title="Clients"
                searchData={(text)=>this.searchData(text)}
                add={() =>{this.setState({isModalVisible:true})}}
                navigation={this.props.navigation}
                isLoading={(this.props.isLoading)&& this.state.isModalVisible==false}
                renderItem={(item)=>{
                  return(<>
                  <ListItem.Content >
                      <Button transparent iconLeft >
                        <Icon style={{color:COLORS.primaire}}  active name="md-person" />
                        <Text style={{color:COLORS.primaire}}>{item.email}</Text>
                      </Button>
                  </ListItem.Content>
                  <ListItem.Chevron color="white" />
                  </>)
                }}
              />
              <Modal isVisible={this.state.isModalVisible} style={style.modal} 
                    animationIn="slideInUp" animationOut="slideOutDown" 
                    onBackdropPress={() =>{
                      this.arrayholder=this.props.clients
                      this.setState({isModalVisible:false,list:this.props.clients})
                    }
                    }
                    onSwipeComplete={() =>{
                      this.arrayholder=this.props.clients
                      this.setState({isModalVisible:false,list:this.props.clients})}} swipeDirection="right">
                    <NewUser title="Nouveau client" role="client"/>
              </Modal>
            <Footer >
              <FooterT navigation={this.props.navigation} />
            </Footer>
          </ImageBackground>
          </Container>
          </Root>
      );
     }
 
}

const mapStateToProps = (state) => {
    return {
      user:state.auth.user,
      users:state.user.users,
      isLoading:state.user.isLoading,
      clients:state.user.clients
    }
  }
export default connect(mapStateToProps,{getClients,deleteClient})(Clients)
