import React, { Component } from 'react'
import { Text, View ,TextInput} from 'react-native';
import { connect } from 'react-redux'
import {
    Spinner ,
    Button,
    Toast,
  } from 'native-base';
import style from '../assets/styles/newCommandeProduct'
import { addProduct,getProduct,editProduct} from "../redux/product/actions";
import {COLORS } from "../util/constants";


class NewProduct extends Component {
    constructor(props){
        super(props)
        this.prix=this.props.item!==null?this.props.item.prix:''
        this.nom=this.props.item!==null?this.props.item.nom:''
        this.id_produit=this.props.item!==null?this.props.item.id_produit:null

    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    add= ()=>{
        if(this.id_produit===null)
        {
        let successCallBack = () => {
            this.props.getProduct(()=>{alert("Produit bien ajouté") },()=>{});
        };
        let errorCallBack=()=>{
            alert("Erreur")
        }
        if(this.prix!='' && this.nom!="")
        {
            this.props.addProduct(this.nom,this.prix,successCallBack,errorCallBack)
        }
        else{
            alert("Veuillez remplir tout les champs")
        }
    }
        else{
            let successCallBack = () => {
                this.props.getProduct(
                    ()=>{
                        alert('Produit bien modifié')
                    },()=>{});
            };
            let errorCallBack=()=>{
                alert("Erreur, Ressayer!")
            }
            if(this.nom!=this.props.item.nom|| this.prix!=this.props.item.prix)
            {
                this.props.editProduct(this.nom,this.prix,this.id_produit,successCallBack,errorCallBack)
            }
            else{
                //alert("Veuillez remplir tout les champs")
            }
        }
    }
    render() {
        let title=this.id_produit===null?'Nouveau Produit':'Modifier Produit'
        return (
                <View style={style.formview}>
                    <Text style={style.headertext}>{title}</Text>
                    <TextInput placeholder="Nom" defaultValue={this.nom} placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={nom=>this.nom=nom}/>
                    <TextInput placeholder="Prix" defaultValue={this.prix} placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={prix=>this.prix=prix}/>
                    { this.props.isLoading ?
                    <Spinner color={COLORS.primaire}/>
                    : 
                    <Button rounded block style={style.button} onPress={this.add}>
                        <Text style={style.buttontext}>Valider</Text>
                    </Button>
                    }
                    
                </View>
                
        )
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading:state.produit.isLoading
      }
  }
export default connect(mapStateToProps,{addProduct,getProduct,editProduct})(NewProduct)
