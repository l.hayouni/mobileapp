import React, { Component } from 'react'
import { Text, View ,TextInput ,TouchableOpacity,ActivityIndicator} from 'react-native';
import { connect } from 'react-redux'
import {
    Spinner ,
    Button,
    Toast,
  } from 'native-base';
import style from '../assets/styles/newCommandeProduct'
import { addUser,getUsers,addClient,getClients} from "../redux/user/actions";
import {COLORS } from "../util/constants";


class NewUser extends Component {
    constructor(props){
        super(props)
        this.username=''
        this.password=""
    }
    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
      }
    add= ()=>{
        if(this.props.role==="employe")
        {
            let successCallBack = () => {
                this.props.getUsers("employe",()=>{alert("Employé bien ajouté")},()=>{});
            };
            let errorCallBack=()=>{
                alert("Erreur")
            }
            if(this.username!='' && this.password!="")
            {
                this.props.addUser(this.username,this.password,this.props.role,this.props.user.id,successCallBack,errorCallBack)
            }
            else{
                alert("Veuillez remplir tout les champs")
            }
        }
        else{
            let successCallBack = () => {
                this.props.getClients(this.props.user.id,()=>{alert("Client bien ajouté")},()=>{});
            };
            let errorCallBack=()=>{
                alert("Erreur")
            }
            if(this.username!='')
            {
                this.props.addClient(this.username,this.props.user.id,successCallBack,errorCallBack)
            }
            else{
                alert("Veuillez remplir tout les champs")
            }
        }
        
    }
    render() {
        return (
                <View style={style.formview}>
                    <Text style={style.headertext}>{this.props.title}</Text>
                    <TextInput placeholder="Nom d'utilisateur" placeholderTextColor={COLORS.primaire} style={style.textinput} onChangeText={username=>this.username=username}/>
                    {this.props.role=="employe"&&
                    <TextInput placeholder="Mot de passe" placeholderTextColor={COLORS.primaire} secureTextEntry={true} style={style.textinput} onChangeText={password=>this.password=password}/>}
                    { this.props.isLoading ?
                    <Spinner color={COLORS.primaire}/>
                    : 
                    <Button rounded block style={style.button} onPress={this.add}>
                        <Text style={style.buttontext}>Ajouter</Text>
                    </Button>
                    }
                    
                </View>
                
        )
    }
}
const mapStateToProps = (state) => {
    return {
        user:state.auth.user,
        isLoading:state.user.isLoading
      }
  }
export default connect(mapStateToProps,{addUser,getUsers,addClient,getClients})(NewUser)
