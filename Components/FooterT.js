import React, { Component } from 'react'
import { Alert} from 'react-native';
import {
    Icon,
    Footer,
    FooterTab,
    Button,
    Text
  } from 'native-base';
import { logout} from "../redux/auth/actions";
import { connect } from 'react-redux'

class FooterT extends Component {
    logout=()=>{
        Alert.alert("", "Vous voulez se déconnecter?", [
          {
            text: "Non",
            onPress: () => null,
            style: "cancel"
          },
          { text: "Oui", onPress: () => 
            { 
              this.props.logout()
              this.props.navigation.navigate('Login')
             }
        }
        ]);
        
      }
    render() {
        return (
            <FooterTab style={{backgroundColor:'#506FA8'}}>
                <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                  <Icon name="home" style={{color:"white"}} />
                  <Text style={{color:"white",textTransform: 'capitalize'}}>Acceuil</Text>
                </Button>
                <Button vertical onPress={this.logout} >
                  <Icon name="md-log-out" style={{color:"white"}} />
                  <Text style={{color:"white",textTransform: 'capitalize'}}>Deconnexion</Text>
                </Button>
            </FooterTab>
        )
    }
}
const mapStateToProps = (state) => {
    return {
      user:state.auth.user,
    }
  }
export default connect(mapStateToProps,{ logout})(FooterT)